#!/usr/bin/env python
"""
Start new measuring season for upGPR at WFJ.
For this create new folders, copy INI file and update entries in INI files.

"""
#import numpy as np
from datetime import datetime
import os,sys
import datetime
from shutil import copyfile

def subs_txt_file(path,new, old):
    """
    Substitute old text with new text in text file.

    Input:
    ------

    path:       file path
    new:        new text. Can be an iterable with more entries.
    old:        Old text. Can be an iterable with more entries.  Must have the same length as new.

    Output:
    -------
    error:      Error if text was not found.

    """
    error=0
    
    # check inputs
    if type(new)==str and type(old)==str:
        new=[new]
        old=[old]

    if len(new)!=len(old):
        raise ValueError('"new" and "old" don\'t have the same legth or they are not both a simple string.' )

    try: 
        #read input file
        fin = open(path, "rt")
        #read file contents to string
        data = fin.read()
        #close the input file
        fin.close()

        # replace strings
        for n,s in zip(new,old):
            #replace all occurrences of the required string
            data = data.replace(s, n)




        #open the input file in write mode
        fin = open(path, "wt")
        #overrite the input file with the resulting data
        fin.write(data)
        #close the file
        fin.close()
    except:
        error=1

    return error



if __name__ == '__main__':

    # get season
    now = datetime.datetime.now()
    season='{:d}-{:d}'.format(now.year,now.year-1999)
    oldseason='{:d}-{:d}'.format(now.year-1,now.year-2000)

    print('oldseason: ',oldseason)
    print('season: ',season)
    
    # define path
    upGPR=r'C:\batches\upGPR.ini'
    newEvalpath='D:\\scans\\'+season+'\Evaluation'
    oldEvalpath='D:\\scans\\'+oldseason+'\Evaluation'
    
    copyerror=0
    ini_err_upGPR=0
    
    # create new folders for raw data and evaluation
    try:
        os.makedirs(newEvalpath)
    except:
        print(newEvalpath+' already exist.')

    try:
        copyfile(oldEvalpath+'\Eval{:d}.ini'.format(now.year), newEvalpath+'\Eval{:d}.ini'.format(now.year+1))
        copyfile(oldEvalpath+'\staticCorShift.mat', newEvalpath+'\staticCorShift.mat')
    except:
        copyerror=1
    
    # substitute text in upGPR.ini
    ini_err_upGPR=subs_txt_file(upGPR,'season="'+season+'"', 'season="'+oldseason+'"')

    # substitute text in Eval*.ini
    ini_err_Eval=subs_txt_file(newEvalpath+'\Eval{:d}.ini'.format(now.year+1),
                                  ['season="'+season+'"', 'rawdatapath="D:\\scans\\'+season+'"' ],
                                  ['season="'+oldseason+'"', 'rawdatapath="D:\\scans\\'+oldseason+'"' ])
    msg=''
    if copyerror:
        msg+='Could not copy INI files to new location. Creation of New season {} failed.    '.format(season)
    if ini_err_upGPR:
        msg+='Could not make changes in "{:s}". Creation of New season {} failed.   '.format(upGPR,season)
    if ini_err_Eval:
        msg+='Could not make changes in "{:s}". Creation of New season {} failed.  '.format(newEvalpath+'\Eval{:d}.ini'.format(now.year+1),season)
        
    if not copyerror and not ini_err_upGPR and not ini_err_Eval:
        msg+=('Creation of new upGPR season {} was succesful!   Check correct functioning of upGPR data acquisition and automatic evaluation. '.format(season))
        err=2
    else:
        err=0
        
    # send email
    print(msg)
    command= r'C:\batches\SendMail.exe "{}" {}'.format(msg,err)            
    
    os.system(command)   
        
       
        
       
                                