;===============================================================================
; Description:		Performs a radar scan on Weissfluhjoch
;
; Author:			Lino Schmid, SLF Davos
; Modified by:		Achille Capelli, SLF Davos, 2019
;===============================================================================

#include <MailAndSMS.au3>
#include <Date.au3>
#Include <GuiComboBox.au3>
#include <Timers.au3>
#include "CommAPI/CommInterface.au3"

; Flags
Const $b_Test = False
; Titles of windows
Const $s_Station = "Weissfluhjoch"
Const $s_SplashControlText = "AutoIt Measurement"
Const $s_WinK2 = "K2FastWave v02.00.010"
Const $s_WinRadarSetup = "K2FastWave v02.00.010 - RADAR SETUP"
Const $s_WinAcquisition = "K2FastWave v02.00.010 - ACQUISITION SELECTION"
Const $s_WinNewSurvey = "New survey"
Const $s_WinMeas = "K2FastWave v02.00.010 - NEW ACQUISITION"
Const $s_WinAdvantecDM = "Advantech Device Manager"
Const $s_WinUSB = "Advantech Device Test - USB-4761 BoardID=0"
Const $s_WinSavePopup = "K2FastWave"
Const $s_WinSavePopupText = "Save last acquisition?"
; ID's of controls
Const $i_IdStartCalibration = 84
Const $i_IdCurrentSurvey = 54
Const $i_IdNewSurvey = 53
Const $i_IdNewSurveyText = 2
Const $i_IdNewSurveyOK = 3
Const $i_IdNewSurveyCancel = 4
Const $i_IdNewAcquisitionText = 58
Const $i_IdNewAcquisition = 61
Const $i_IdEndAcquisition = 68
Const $i_IdStartScan = 67
Const $i_IdStopScan = 65
Const $i_IdBackToSetup = 88
Const $i_IdSave = 6
Const $i_IdDontSave = 7

; Text of controls
Const $s_IdStartCalibrationText = "Start calibration"
Const $s_IdNewSurveyText = "New survey"
Const $s_IdNewAcquisitionText = "New acquisition"
Const $s_IdEndAcquisitionText = "End acquisition"
Const $s_IdStartScanText = "Start scan"
Const $s_IdStopScanText = "Stop scan"
Const $s_IdBackToSetupText = "Back to setup"
Const $s_IdSaveText = "&Yes"
Const $s_IdDontSaveText = "&No"

; Constant numbers
Const $s_MinFreeSpaceC = 10*86.7
Const $s_MinFreeSpaceD = 10*1000
Const $i_NrOfMeasurements = 1
Const $i_WaitSec = 5
Const $i_MaxNrOfAttempts = 5
Const $i_MaxRepetitionTimesRadar = 5
Const $t_maxRepititionTime = 15*60*1000

; time to wait for motor moving up/down or waiting in position. In ms.
Const $i_SleepUp = 9000
Const $i_SleepDown = 9000
Const $i_SleepTop = 4000
Const $i_SleepBottom = 4000

; Strings
Const $s_LogError = "Could not open log-file."
; Directories/Files
Const $s_DirK2 = "C:\K2FastWave\mission\"
Const $s_DirAutoIt = "C:\Users\Admin\Desktop\batches\"
Const $s_DirScans = "D:\scans\"

; Paths to Programs
Const $s_PathK2 = "C:\K2FastWave\K2FastWave"




;Constants for Motor via Numato
Const $i_ComPortNumato = 3      ; Constants for COM
Const $i_GPIO1 = 4
Const $i_GPIO2 = 5
Const $i_NumatoMinTimeBetweenTwoCalls = 500
; Const $s_NumatoAllInputs = "gpio iodir ff" & @CR
; Const $s_NumatoAdcRead24V = "adc read 4" & @CR
; Const $s_NumatoAdcRead12V = "adc read 5" & @CR
; Const $s_NumatoDeviceID = "*VID_2A19*"
; initialise variables



; ----------------------------------------------------
; Make radar measurement
; ----------------------------------------------------
	
$b_RadarMeasurementSuccessfull = False
$s_ErrorMessageForSMS = ""
$s_ErrorMessageForEmail = ""
$i_AttemptRadar = 0
$t_starttime = _Timer_Init()
$t_lastNumato = _Timer_Init()
$hPortNumato = 0


_PopupText("")
_WriteToLogFile("Start new measurement", 1)
WinMove($s_WinTask, "", 0, 0)


Do
	; Actual date
	Const $s_Season = "2019-20"
	Const $s_YEAR = @YEAR
	Const $s_MON = @MON
	Const $s_DAY = @MDAY
	Const $s_HOUR = @HOUR
	Const $s_MIN = @MIN
	Const $s_now = $s_YEAR & "." & $s_MON  & "." & $s_DAY & " " & $s_HOUR & ":" & $s_MIN


    $cSplashWindow = SplashTextOn($s_SplashControlText, "", -1, -1, -1, -1, 4+16, "", 24)
	$i_AttemptRadar=$i_AttemptRadar+1
	_PopupText("Start new radar measurement, attempt " & $i_AttemptRadar)
	_WriteToLogFile("Start new radar measurement, attempt " & $i_AttemptRadar)
	
	; ----------------------------------------------------
	; Show popup window and write first entry to logfile
	; ----------------------------------------------------
	;BlockInput(1)
	$cSplashWindow = SplashTextOn($s_SplashControlText, "", -1, -1, -1, -1, 4+16, "", 24)
	_PopupText("")
	_WriteToLogFile("Start new measurement", 1)

	; ----------------------------------------------------
	; check if Radar already in use
	; ----------------------------------------------------
	_WriteToLogFile("check if Radar already in use")
	If WinExists($s_WinMeas) And Not ControlCommand($s_WinMeas, $s_IdStartScanText, $i_IdStartScan, "IsEnabled", "") And (ControlCommand($s_WinMeas, $s_IdStopScanText, $i_IdStopScan, "IsEnabled", "") Or WinExists($s_WinSavePopup, $s_WinSavePopupText)) Then
		_WriteToLogFile("AutoIt tried to make a measurement. Radar already in use. Measurement abandoned.", 3)
		$s_MailSmsError = _SendMailAndSMS($s_Station, "Radar already in use.")
		$i_errorCode = @error
		If Not (BitAND($i_errorCode,7) = 0) Then
			_WriteToLogFile("Could not send Mail and SMS." & @CRLF & $s_MailSmsError, 3)
		ElseIf (BitAND($i_errorCode,8) = 8) Then
			_WriteToLogFile("Could send Mail and SMS, but " & $s_MailSmsError)
		EndIf
		SplashOff()
		MsgBox( 0x1010, "Error", "AutoIt tried to make a measurement. Measurement abandoned.")
		Exit
	 EndIf

	; ----------------------------------------------------
	; Initialize Numato and move radar down
	; ----------------------------------------------------
	_WriteToLogFile("Set COM Port for Numato and  move radar down.")
	_PopupText("Setting COM port for Numato and moving radar down.")
	;open com port
	$hPortNumato = _CommAPI_OpenCOMPort($i_ComPortNumato, 9600, 0, 8, 1)
	If @error Then
		_WriteToLogFile("Numato doesn't work. Unable to open COM port.", 3)
		_AddMessageToQueue("Numato doesn't work. Unable to open COM port.")
		ContinueLoop
		; Code to reset Numato
		; If WinExists($s_WinResetNumatoCMD) Then
			; _WriteToLogFile("Reset Numato was not successful.", 3)
			; Exit
		; Else
			; _WriteToLogFile("Numato doesn't work. Restart it.", 3)
			; _CommAPI_ClosePort($hPortNumato)
			; Run($s_resetNumatoBat)
			; Exit
		; EndIf
	EndIf
	_NumatoMotorDown($hPortNumato, $i_GPIO1, $i_GPIO2)
	If @error Then
		_AddMessageToQueue("Unable to set Motor to 'Down' (Error " & @error & "): " & _WinAPI_GetLastErrorMessage() ,"")
		_WriteToLogFile("Unable to set Motor to 'Down' (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(),3)
	EndIf
	Sleep($i_SleepDown)


	; ----------------------------------------------------
	; check if there is enough free space on the drives
	; ----------------------------------------------------
	_WriteToLogFile("Check if enough free space on the drives")
	; Get free space
	$FreeSpaceC = DriveSpaceFree ("C:\")
	If @error Then
		_StopAndExit("Drive C:\ is not available.")
	 EndIf
	$FreeSpaceD = DriveSpaceFree ("D:\")
	If @error Then
		_StopAndExit("Drive D:\ is not available.")
	 EndIf
	 ; Check if free space on C: is enougth and hadle according
	If $FreeSpaceC<$s_MinFreeSpaceC Then
		$spceMsg = "There are only " & Round($FreeSpaceC,2) & "MB free space on C:"
		_WriteToLogFile("Free space on drive is low. " & $spceMsg, 4)
		$s_MailSmsError = _SendMailAndSMS($s_Station, "Free space on drive is low. " & $spceMsg, 1)
		$i_errorCode = @error
		If Not (BitAND($i_errorCode,7) = 0) Then
			_WriteToLogFile("Could not send Mail and SMS." & @CRLF & $s_MailSmsError, 3)
		ElseIf (BitAND($i_errorCode,8) = 8) Then
			_WriteToLogFile("Could send Mail and SMS, but " & $s_MailSmsError)
		EndIf
	Else
		_WriteToLogFile("Free space ok. There are " & Round($FreeSpaceC,2) & "MB free space on C:")
	EndIf
	 ; Check if free space on D: is enougth and hadle according
	If $FreeSpaceD<$s_MinFreeSpaceD Then
		$spceMsg = "There are only " & Round($FreeSpaceD,2) & "MB free space on D:"
		_WriteToLogFile("Free space on drive is low. " & $spceMsg, 4)
		$s_MailSmsError = _SendMailAndSMS($s_Station, "Free space on drive is low. " & $spceMsg, 1)
		$i_errorCode = @error
		If Not (BitAND($i_errorCode,7) = 0) Then
			_WriteToLogFile("Could not send Mail and SMS." & @CRLF & $s_MailSmsError, 3)
		ElseIf (BitAND($i_errorCode,8) = 8) Then
			_WriteToLogFile("Could send Mail and SMS, but " & $s_MailSmsError)
		EndIf
	Else
		_WriteToLogFile("Free space ok. There are " & Round($FreeSpaceD,2) & "MB free space on D:")
	EndIf


	; ----------------------------------------------------
	; Start K2FastWafe
	; ----------------------------------------------------
	$i_NrOfAttempts = 0
	$b_attemptSuccessful  = False
	Do
		$i_NrOfAttempts = $i_NrOfAttempts + 1
		_PopupText("Start K2FastWave, attempt " & $i_NrOfAttempts)
		_WriteToLogFile("Start K2FastWave, attempt " & $i_NrOfAttempts)
		; close K2 if it is startet but we dont know in which window he is
		If WinExists($s_WinK2) And Not WinExists($s_WinRadarSetup) And Not WinExists($s_WinMeas) And Not WinExists($s_WinAcquisition) Then
			WinClose($s_WinK2)
			WinWaitClose ($s_WinK2, "", $i_WaitSec)
		EndIf
		; start K2 if not already started
		If Not WinExists($s_WinK2) Then
			_WriteToLogFile("Start K2 (it is not already started)")
			Run($s_PathK2)
			$f_Time=0
			While Not ControlCommand($s_WinK2, $s_IdStartCalibrationText, $i_IdStartCalibration, "IsEnabled", "") And $f_Time<15
				Sleep(200)
				$f_Time+=0.200
			WEnd
		EndIf
		WinActivate ( $s_WinK2 )
		WinMove($s_WinK2, "", 475, 0)
		; go to radar setup window
		_WriteToLogFile("Go to radar setup window")
		If WinExists($s_WinMeas) Then
			_WriteToLogFile("Window s_WinMeas exists")
			ControlFocus ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )
			ControlClick ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )
			WinWaitActive($s_WinAcquisition, "", $i_WaitSec)
		EndIf
		If WinExists($s_WinAcquisition) Then
			_WriteToLogFile("Window $s_WinAcquisition exists")
			If WinExists($s_WinNewSurvey) Then
				_WriteToLogFile("Window $s_WinNewSurvey exists")
				ControlFocus ($s_WinNewSurvey, "", $i_IdNewSurveyCancel )
				ControlClick ($s_WinNewSurvey, "", $i_IdNewSurveyCancel )
			EndIf
			_WriteToLogFile("Go back to Setup")
			ControlFocus ($s_WinAcquisition, $s_IdBackToSetupText, $i_IdBackToSetup )
			ControlClick ($s_WinAcquisition, $s_IdBackToSetupText, $i_IdBackToSetup )
			WinWaitActive($s_WinRadarSetup, "", $i_WaitSec)
		EndIf
		If Not WinExists($s_WinRadarSetup) Then
			_WriteToLogFile("Window Radar Setup is not displayed.", 4)
			ContinueLoop
			;_StopAndExit("Window Radar Setup is not displayed.")
		EndIf
		_WriteToLogFile("Start Calibration")
		ControlFocus ($s_WinRadarSetup, $s_IdStartCalibrationText, $i_IdStartCalibration )
		ControlClick ($s_WinRadarSetup, $s_IdStartCalibrationText, $i_IdStartCalibration )
		WinWaitActive($s_WinAcquisition, "", 30)
		If Not WinExists($s_WinAcquisition) Then
			_WriteToLogFile("Window Acquisition is not displayed.", 4)
			ContinueLoop
			;_StopAndExit("Window Acquisition is not displayed.")
		EndIf

		; make new survey if necessary
		_WriteToLogFile("Make new survey if necessary")
		$s_ActSurvey = ControlCommand ($s_WinAcquisition,"",$i_IdCurrentSurvey,"GetCurrentSelection", "")
		$s_Survey = StringMid($s_YEAR,3,2) & $s_MON & $s_DAY
		If Not (StringMid($s_ActSurvey,10) = $s_Survey) Then
			_WriteToLogFile("Actual survey is not desired survey")
			$h_ComboboxSurvey = ControlGetHandle ( $s_WinAcquisition, "", $i_IdCurrentSurvey )
			$l_SurveyHistory = _GUICtrlComboBox_GetListArray($h_ComboboxSurvey)
			$i_ActSurveyInd = -1
			For $i = 1 To $l_SurveyHistory[0]
				If StringMid($l_SurveyHistory[$i],10) = $s_Survey Then
					$i_ActSurveyInd = $i - 1
					ExitLoop
				EndIf
			Next
			If $i_ActSurveyInd=-1 Then
				_WriteToLogFile("Desired survey does not already exists")
				ControlFocus ($s_WinAcquisition, $s_IdNewSurveyText, $i_IdNewSurvey )
				ControlClick ($s_WinAcquisition, $s_IdNewSurveyText, $i_IdNewSurvey )
				WinWaitActive($s_WinNewSurvey, "", $i_WaitSec)
				If Not WinExists($s_WinNewSurvey) Then
					_WriteToLogFile("Window New survey does not appear.", 4)
					ContinueLoop
					;_StopAndExit("Window New survey does not appear.")
				EndIf
				ControlFocus ($s_WinNewSurvey, "", $i_IdNewSurveyText )
				ControlSetText  ($s_WinNewSurvey, "", $i_IdNewSurveyText, $s_Survey)
				ControlFocus ($s_WinNewSurvey, "", $i_IdNewSurveyOK )
				If Not ControlCommand( $s_WinNewSurvey, "", $i_IdNewSurveyOK, "IsEnabled", "") Then
					_WriteToLogFile("Survey " & $s_Survey & " already exists.", 4)
					ContinueLoop
					;_StopAndExit("Survey " & $s_Survey & " already exists.")
				EndIf
				ControlClick ($s_WinNewSurvey, "", $i_IdNewSurveyOK )
			Else
				_WriteToLogFile("Desired survey already exists. Select it")
				ControlCommand( $s_WinAcquisition, "", $i_IdCurrentSurvey, "SetCurrentSelection", $i_ActSurveyInd)
			EndIf
		EndIf
		$s_ActSurvey = ControlCommand ($s_WinAcquisition,"",$i_IdCurrentSurvey,"GetCurrentSelection", "")
		If Not (StringMid($s_ActSurvey,10) = $s_Survey) Then
			_WriteToLogFile("Can not create new survey.", 4)
			ContinueLoop
			;_StopAndExit("Can not create new survey.")
		EndIf
		; make new acquisition
		_WriteToLogFile("Make new acquisition")
		$s_Acquisition = $s_Survey & "_" & $s_HOUR & $s_MIN
		ControlSetText( $s_WinAcquisition, "", $i_IdNewAcquisitionText, $s_Acquisition )
		If Not ControlCommand($s_WinAcquisition, $s_IdNewAcquisitionText, $i_IdNewAcquisition, "IsEnabled", "") Then
			_WriteToLogFile("Acquisition " & $s_Acquisition & " already exist in survey " & $s_Survey, 4)
			ContinueLoop
			;_StopAndExit("Acquisition " & $s_Acquisition & " already exist in survey " & $s_Survey)
		EndIf
		ControlFocus ($s_WinAcquisition, $s_IdNewAcquisitionText, $i_IdNewAcquisition )
		ControlClick ($s_WinAcquisition, $s_IdNewAcquisitionText, $i_IdNewAcquisition )
		WinWaitActive($s_WinMeas, "", $i_WaitSec)

		If Not WinExists($s_WinMeas) Then
			_WriteToLogFile("Window new acquisition could not be displayed.", 4)
			ContinueLoop
			;_StopAndExit("Window new acquisition could not be displayed.")
		EndIf
		$b_attemptSuccessful = True
	Until $b_attemptSuccessful Or ($i_NrOfAttempts>=$i_MaxNrOfAttempts)
	If Not $b_attemptSuccessful Then
		_StopAndExit("Software K2FastWave could even not be started after " & $i_MaxNrOfAttempts & " attempts.")
	EndIf



	; ----------------------------------------------------
	; start scan
	; ----------------------------------------------------
	_PopupText("Start scanning")
	_WriteToLogFile("Start scanning")
	If Not WinExists($s_WinMeas) Then
		_StopAndExit("Measurement window does not exist.")
	EndIf
	ControlFocus ( $s_WinMeas, $s_IdStartScanText, $i_IdStartScan )
	ControlClick ( $s_WinMeas, $s_IdStartScanText, $i_IdStartScan )


	Sleep($i_SleepBottom)

	; ----------------------------------------------------
	; move radar up and down
	; ----------------------------------------------------
	For $j = 1 to $i_NrOfMeasurements
		; move radar up
		_PopupText("Round " & $j & ": Move radar up.")
		_WriteToLogFile("Round " & $j & ": Move radar up.")
		_NumatoMotorUp($hPortNumato, $i_GPIO1, $i_GPIO2)
		If @error Then
			_AddMessageToQueue("Unable to set Motor to 'Up' (Error " & @error & "): " & _WinAPI_GetLastErrorMessage()  )
			_WriteToLogFile("Unable to set Motor to 'Up' (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(),3)
		EndIf
		Sleep($i_SleepUp)

		; Wait on top
		_PopupText("Round " & $j & ": Wait on upper position.")
		_WriteToLogFile("Round " & $j & ": Wait on upper position.")
		Sleep($i_SleepTop)

		; move radar down
		_PopupText("Round " & $j & ": Move radar down.")
		_WriteToLogFile("Round " & $j & ": Move radar down.")
		_NumatoMotorDown($hPortNumato, $i_GPIO1, $i_GPIO2)
		If @error Then
			_AddMessageToQueue("Unable to set Motor to 'Down' (Error " & @error & "): " & _WinAPI_GetLastErrorMessage()  )
			_WriteToLogFile("Unable to set Motor to 'Down' (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(),3)
		EndIf
		Sleep($i_SleepDown)

		;wait on bottom
		_PopupText("Round " & $j & ": Wait on lower position.")
		_WriteToLogFile("Round " & $j & ": Wait on lower position.")
		Sleep($i_SleepBottom)
	Next

	; ----------------------------------------------------
	; stop scan
	; ----------------------------------------------------
	_PopupText("Stop scan and save")
	_WriteToLogFile("Stop scan")
	If Not WinExists($s_WinMeas) Then
		_StopAndExit("Measurement window does not exist.")
	EndIf
	ControlFocus ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
	ControlClick ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )

	; ----------------------------------------------------
	; wait for save-popup and press yes
	; ----------------------------------------------------
	_WriteToLogFile("Save scan")
	WinWait($s_WinSavePopup, $s_WinSavePopupText, $i_WaitSec)
	If Not WinExists($s_WinSavePopup, $s_WinSavePopupText) Then
		_StopAndExit("Save window does not appear.")
	EndIf
	If Not $b_Test Then
		ControlFocus ($s_WinSavePopup, $s_IdSaveText, $i_IdSave )
		ControlClick ( $s_WinSavePopup, $s_IdSaveText, $i_IdSave )
	Else
		ControlFocus ( $s_WinSavePopup, $s_IdDontSaveText, $i_IdDontSave )
		ControlClick ( $s_WinSavePopup, $s_IdDontSaveText, $i_IdDontSave )
	EndIf

	; End acquisition
	_WriteToLogFile("End acquisition")
	ControlFocus ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )
	ControlClick ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )


	; ----------------------------------------------------
	; wait until files surely written
	; ----------------------------------------------------
	_PopupText("Wait until Files are written.")
	_WriteToLogFile("Wait until Files are written.")
	Sleep(10000)

	; ----------------------------------------------------
	; Close K2
	_WriteToLogFile("Close K2")
	WinClose($s_WinK2)
	; ----------------------------------------------------


	; ----------------------------------------------------
	; Switch motor power off and close numato port
	; ----------------------------------------------------
	_NumatoMotorOff($hPortNumato, $i_GPIO1, $i_GPIO2)
	If @error Then
		_AddMessageToQueue("Unable to set switch motor power off (Error " & @error & "): " & _WinAPI_GetLastErrorMessage()  )
		_WriteToLogFile("Unable to set switch motor power off (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(),3)
	EndIf
	_WriteToLogFile("Close Port for Numato.")
	_CommAPI_ClosePort($hPortNumato)
	If @error Then
		_AddMessageToQueue("Can not close port for Numato: " & _WinAPI_GetLastErrorMessage()  )
		_WriteToLogFile("Can not close port for Numato: " & _WinAPI_GetLastErrorMessage() , 3)
	EndIf




	; ----------------------------------------------------
	; copy files
	; ----------------------------------------------------
	_PopupText("Copy files to D:\")
	; _WriteToLogFile("Copy files to D:\")
	If Not $b_Test Then
		$s_ActDir = $s_DirK2 & $s_Survey & ".Mis\"
		$s_NewDir = $s_DirScans  & $s_Season & "\" & $s_Survey & ".Mis\"

		; check if source exist
		$search = FileExists($s_ActDir)
		If $search = 0 Then
			_StopAndExit("Folder:" & $s_ActDir & " doesn't exist. Can't move files to D:." )
		EndIf
		FileClose($search)
		;check if .dt file exist
		$search = FileFindFirstFile($s_ActDir& $s_Acquisition & ".ZON\" & "*.dt")
		If $search = -1 Then
			_StopAndExit("No *.dt-File found in " & $s_ActDir& $s_Acquisition & ".ZON\")
		EndIf
		FileClose($search)

		; move dir
		; $move=DirMove($s_ActDir, $s_NewDir,0)
		$move=RunWait('robocopy  /e /move  ' & $s_ActDir & ' ' & $s_NewDir)
		_WriteToLogFile($move)
		If $move = 1 or $move = 3 Then
			_PopupText("Files copied successfully to D:\")
			_WriteToLogFile("Files copied successfully to D:\")
		ElseIf $move = 0 Then
			_StopAndExit("Could not move dir:" & $s_ActDir & " to dir: " & $s_NewDir& ". Robocopy error:" & $move)
		ELseif $move = 8 Then
			_StopAndExit("Could not move dir:" & $s_ActDir & " to dir: " & $s_NewDir& ". Robocopy error:" & $move)
		ELseif $move > 8 Then
			_StopAndExit("Could not move dir:" & $s_ActDir & " to dir: " & $s_NewDir& ". Robocopy error:" & $move)
		Else
			_WriteToLogFile("Files copied to D, but error encoutered. Robocopy error:" & $move)
		EndIf
	EndIf


	$b_RadarMeasurementSuccessfull = True
	; check if mesurement was successful and retry if time or number of attemps was not exceeded 
Until $b_RadarMeasurementSuccessfull Or (_Timer_Diff($t_starttime)>$t_maxRepititionTime) Or ($i_AttemptRadar > $i_MaxRepetitionTimesRadar)


; ----------------------------------------------------
; send error messages
; ----------------------------------------------------
_WriteToLogFile("Check if there is a message to send over SMS or EMail.")
_PopupText("Check if there is a message to send over SMS or EMail.")
If ($b_RadarMeasurementSuccessfull) Then
    ; if both measurements successful, only send mail (no SMS)
	If (Not ($s_ErrorMessageForEmail = "")) Then
		_SendMailAndSMS("Radar measurements could be done successfully, but with errors:" & @CRLF & $s_ErrorMessageForEmail & @CRLF & @CRLF & "Attempts: " & $i_AttemptRadar & ", Time: " & _Timer_Diff($t_starttime)/1000/60 & " min.", 1, True, False, 0)
	Endif
Else
	_SendMailAndSMS( "Radar Measurement could not be done successfully. Reason is:" & @CRLF & $s_ErrorMessageForSMS, 0, False, True, 0)
	_SendMailAndSMS( "Radar Measurement could not be done successfully. Reason is:" & @CRLF & $s_ErrorMessageForEmail & @CRLF & @CRLF & "Attempts: " & $i_AttemptRadar & ", Time: " & _Timer_Diff($t_starttime)/1000/60 & " min.", 0, True, False, 0)
EndIf


; ------------------------------------------------------------
; send confirmation that measurement has successfully be done
; ------------------------------------------------------------
_PopupText("Measurement successfully done")
_WriteToLogFile("Measurement successfully done")
BlockInput(0)
SplashOff()

Exit
;------------------------------------------------------------------
;End
;------------------------------------------------------------------





;------------------------------------------------------
;------------------------------------------------------
; Functions definition
;------------------------------------------------------
;------------------------------------------------------



;============================================================================
; Function Name:   _NumatoSend($p_Port, $s_comm)

; Description:    Read something from Numato
; Parameters:     $p_Port 	 numatoport
;				  $s_comm    String to send to numato
;
; @error    on success-	     Received string
;			on failure-      error from _CommAPI_TransmitString
;===============================================================================
Func _NumatoSend($p_Port, $s_comm)
	;_WriteToLogFile("Numato: " & @SEC & "." & @MSEC)
	$i_sinceLastCall = _Timer_Diff($t_lastNumato)
	If $i_sinceLastCall<$i_NumatoMinTimeBetweenTwoCalls Then
		Sleep($i_NumatoMinTimeBetweenTwoCalls-$i_sinceLastCall)
	EndIf
	$s_ret=_CommAPI_TransmitString($p_Port, $s_comm)
	$i_errorZS = @error
	$t_lastNumato = _Timer_Init()
	If $i_errorZS Then
	   SetError($i_errorZS)
	EndIf
	Return $s_ret
EndFunc   ;==>_NumatoSend





;============================================================================
; Function Name:   _NumatoMotorDown($p_Port, $i_GPIO1, $i_GPIO2)

; Description:    Move motor to lower position
; Parameters:     $p_Port 	 numatoport
;				  $i_GPIO1   GPIO to switch to 0
;				  $i_GPIO2   GPIO to switch to 1
;
; @error    on success-	     Received string
;			on failure-      error from _CommAPI_TransmitString
;===============================================================================
Func _NumatoMotorDown($p_Port, $i_GPIO1, $i_GPIO2)
	;_WriteToLogFile("Numato: " & @SEC & "." & @MSEC)
	$i_sinceLastCall = _Timer_Diff($t_lastNumato)
	If $i_sinceLastCall<$i_NumatoMinTimeBetweenTwoCalls Then
		Sleep($i_NumatoMinTimeBetweenTwoCalls-$i_sinceLastCall)
	EndIf
	$s_ret=_CommAPI_TransmitString($p_Port, "gpio clear "& $i_GPIO1 & @CR)
	Sleep($i_NumatoMinTimeBetweenTwoCalls)
	$s_ret2=_CommAPI_TransmitString($p_Port, "gpio set "& $i_GPIO2 & @CR)
		$i_errorZS = @error
	$t_lastNumato = _Timer_Init()
	If $i_errorZS Then
	   SetError($i_errorZS)
	EndIf
	Return $s_ret &", "& $s_ret2
EndFunc   ;==>_NumatoSend

;============================================================================
; Function Name:   _NumatoMotorUp($p_Port, $i_GPIO1, $i_GPIO2)

; Description:    Move motor to upper position
; Parameters:     $p_Port 	 numatoport
;				  $i_GPIO1   GPIO to switch to 0
;				  $i_GPIO2   GPIO to switch to 1
;
; @error    on success-	     Received string
;			on failure-      error from _CommAPI_TransmitString
;===============================================================================
Func _NumatoMotorUp($p_Port, $i_GPIO1, $i_GPIO2)
	;_WriteToLogFile("Numato: " & @SEC & "." & @MSEC)
	$i_sinceLastCall = _Timer_Diff($t_lastNumato)
	If $i_sinceLastCall<$i_NumatoMinTimeBetweenTwoCalls Then
		Sleep($i_NumatoMinTimeBetweenTwoCalls-$i_sinceLastCall)
	EndIf
	$s_ret=_CommAPI_TransmitString($p_Port, "gpio set "& $i_GPIO1 & @CR)
	Sleep($i_NumatoMinTimeBetweenTwoCalls)
	$s_ret2=_CommAPI_TransmitString($p_Port, "gpio clear "& $i_GPIO2 & @CR)
	$i_errorZS = @error
	$t_lastNumato = _Timer_Init()
	If $i_errorZS Then
	   SetError($i_errorZS)
	EndIf
	Return $s_ret &", "& $s_ret2
EndFunc   ;==>_NumatoSend



;============================================================================
; Function Name:   _NumatoMotorOff($p_Port, $i_GPIO1, $i_GPIO2)

; Description:    Switch off tension of motor (0,0)
; Parameters:     $p_Port 	 numatoport
;				  $i_GPIO1   GPIO to switch to 0
;				  $i_GPIO2   GPIO to switch to 0
;
; @error    on success-	     Received string
;			on failure-      error from _CommAPI_TransmitString
;===============================================================================
Func _NumatoMotorOff($p_Port, $i_GPIO1, $i_GPIO2)
	;_WriteToLogFile("Numato: " & @SEC & "." & @MSEC)
	$i_sinceLastCall = _Timer_Diff($t_lastNumato)
	If $i_sinceLastCall<$i_NumatoMinTimeBetweenTwoCalls Then
		Sleep($i_NumatoMinTimeBetweenTwoCalls-$i_sinceLastCall)
	EndIf
	$s_ret=_CommAPI_TransmitString($p_Port, "gpio clear "& $i_GPIO1 & @CR)
	Sleep($i_NumatoMinTimeBetweenTwoCalls)
	$s_ret2=_CommAPI_TransmitString($p_Port, "gpio clear "& $i_GPIO2 & @CR)
	$i_errorZS = @error
	$t_lastNumato = _Timer_Init()
	If $i_errorZS Then
	   SetError($i_errorZS)
	EndIf
	Return $s_ret &", "& $s_ret2
EndFunc   ;==>_NumatoSend



;===============================================================================
; Function			_WriteToLogFile($s_inputStr)
;
; Description:		Write something to logfile
;
; Parameter(s):		$s_inputStr: String to write to logfile
;					$i_mode:	1: as a title
;								2: normalLogEntry
;								3: error
;								4: unplanned incident (unexpected, but program can solve problem and continue)
;								default: 2
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _WriteToLogFile($s_inputStr, $i_mode=2)
	$file = FileOpen("AutoitLog.txt", 1)
	; Check If file opened for writing
	If $file = -1 Then
		_StopAndExit($s_LogError)
	EndIf

	$s_inputStr = StringReplace($s_inputStr,@CRLF," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@CR," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@LF," ./. ")
	$s_timestamp = @MDAY & "." & @MON & "." & @YEAR & " " & @Hour & ":" & @Min & ":" & @SEC
	Switch $i_mode
		Case 1
			$s_inputStr = @CRLF & "  ++++++++++++++++++++++++++++++++++++++" & @CRLF & "  + " & $s_timestamp & @CRLF & "  + " & $s_inputStr & @CRLF & "  ++++++++++++++++++++++++++++++++++++++"
		Case 2
			$s_inputStr = "[" & $s_timestamp & " M] " & $s_inputStr
		Case 3
			$s_inputStr = "[" & $s_timestamp & " E] " & "E R R O R : " & $s_inputStr
		Case 4
			$s_inputStr = "[" & $s_timestamp & " U] " & "UNPLANNED INCIDENT: " & $s_inputStr
	EndSwitch

	FileWriteLine($file, $s_inputStr)
	FileClose($file)
EndFunc

;===============================================================================
; Function			_PopupText($s_ActualAction)
;
; Description:		Update Text to inform user that and which measurement is
;					in process
;
; Parameter(s):		$s_ActualAction: Action that is carried out at the moment
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _PopupText($s_ActualAction)
	$s_Message = "A measurement is in progress. Please do not use mouse or keyboard." & @CRLF & @CRLF & "Actual action: " & @CRLF & $s_ActualAction
	ControlSetText($s_SplashControlText, "", "Static1", $s_Message)
EndFunc

;===============================================================================
; Function        _StopAndExit([$s_ErrorMessage])
;
; Description:		This method can be called if an error occurs.
;					If $s_ErrorMessage not "", an error-message is sent over mail
;					and over SMS.
;					If $s_ErrorMessage not "", a message with the error-message is displayed
;					A running measurement is stopped
;					Exit from program
;
; Parameter(s):		$s_ErrorMessage:	Error-message or "" if no advices over mail,
;										SMS and Messagebox is desired
;										Default: ""
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _StopAndExit($s_ErrorMessage = "")
	; send errorMessage over SMS and Mail
	If Not $s_ErrorMessage="" Then
		$s_MailSmsError = _SendMailAndSMS($s_Station, $s_ErrorMessage)
		$i_errorCode = @error
	    If Not (BitAND($i_errorCode,7) = 0) Then
		    _WriteToLogFile("Could not send Mail and SMS." & @CRLF & $s_MailSmsError, 3)
	    ElseIf (BitAND($i_errorCode,8) = 8) Then
		    _WriteToLogFile("Could send Mail and SMS, but " & $s_MailSmsError)
	    EndIf
	EndIf
	; log error
	If Not ($s_ErrorMessage = $s_LogError) Then
		 _WriteToLogFile("Program stops with error message: " & $s_ErrorMessage, 3)
	 EndIf
	; stop scan
	If WinExists($s_WinMeas) And ControlCommand($s_WinMeas, $s_IdStopScanText, $i_IdStopScan, "IsEnabled", "") Then
		ControlFocus ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
		ControlClick ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
		; wait for save-popup and press no
		WinWait($s_WinSavePopup, $s_WinSavePopupText, $i_WaitSec)
		If WinExists($s_WinSavePopup, $s_WinSavePopupText) Then
			ControlClick ( $s_WinSavePopup, $s_IdDontSaveText, $i_IdDontSave )
		EndIf
	EndIf
;~ 	; stop logging ultrasonics
;~ 	_StartSingleRS232DataLogging($s_UsCom1, $s_UsFile1, $s_PathRS232DataLogger)
	; show error message
	If Not $s_ErrorMessage="" Then
		SplashOff()
		MsgBox( 0x1010, "Error", "AutoIt abandoned the measurement." & @CRLF & @CRLF & $s_ErrorMessage, 3*$i_WaitSec)
	EndIf
	; exit
	Exit
EndFunc

;===============================================================================
; Function        _AddMessageToQueue([$s_MessageMail [, $s_MessageSMS]])
;
; Description:		Add To Message queue. At the end, an Email or SMS is send.
;					If $s_MessageMail not "", the message is added to Mail-queue.
;					If $s_MessageSMS not "", he message is added to Mail-queue.
;
; Parameter(s):		$s_MessageMail:	Message to be added to Mail queue
;										Default: ""
;             		$s_MessageSMS:	Message to be added to SMS queue
;										Default: ""
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _AddMessageToQueue($s_MessageMail = "", $s_MessageSMS = "")
	If Not $s_MessageMail="" Then
		$s_ErrorMessageForEmail = $s_ErrorMessageForEmail & $s_MessageMail & @CRLF
	EndIf
	If Not $s_MessageSMS="" Then
		$s_ErrorMessageForSMS = $s_ErrorMessageForSMS & $s_MessageSMS & ", "
	EndIf
EndFunc ;==>_AddMessageToQueue