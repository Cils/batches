;===============================================================================
; Description:		Performs a radar scan on Dorfberg
; 
; Author:			Lino Schmid, SLF Davos
;
; Modified:			
;
;===============================================================================

#include <MailAndSMS.au3>
#include 'CommMG.au3'
#include <Date.au3>
#Include <GuiComboBox.au3>
#include <Constants.au3>
#include <Timers.au3>
#include <Array.au3>
#include "CommAPI/CommInterface.au3"

; Titles of windows
Const $s_Station = "Strela"
Const $s_SplashControlText = "AutoIt Measurement"
Const $s_WinK2 = "K2FastWave v02.00.010"
Const $s_WinRadarSetup = "K2FastWave v02.00.010 - RADAR SETUP"
Const $s_WinAcquisition = "K2FastWave v02.00.010 - ACQUISITION SELECTION"
Const $s_WinNewSurvey = "New survey"
Const $s_WinMeas = "K2FastWave v02.00.010 - NEW ACQUISITION"
Const $s_WinSavePopup = "K2FastWave"
Const $s_WinSavePopupText = "Save last acquisition?"
Const $s_WinTask = "Task"
Const $s_ProcessK2 = "K2FastWave.exe"
Const $s_WinSiRFLive = "SiRFLive 2.03P1 Marketing"
Const $s_WinSiRFLiveConnect = "Rx Port Settings"
Const $s_WinSiRFLiveSwitchCommSettings = "Switch Comm Settings"
Const $s_WinSiRFLiveUserDefinedMEssage = "User Defined Message"
Const $s_WinResetNumatoCMD = "ResetNumatoCMD"
; ID's of controls
Const $i_IdStartCalibration = 84
Const $i_IdCurrentSurvey = 54
Const $i_IdNewSurvey = 53
Const $i_IdNewSurveyText = 2
Const $i_IdNewSurveyOK = 3
Const $i_IdNewSurveyCancel = 4
Const $i_IdNewAcquisitionText = 58
Const $i_IdNewAcquisition = 61
Const $i_IdEndAcquisition = 68
Const $i_IdStartScan = 67
Const $i_IdStopScan = 65
Const $i_IdBackToSetup = 88
Const $i_IdSave = 6
Const $i_IdDontSave = 7
; Text of controls
Const $s_IdStartCalibrationText = "Start calibration"
Const $s_IdNewSurveyText = "New survey"
Const $s_IdNewAcquisitionText = "New acquisition"
Const $s_IdEndAcquisitionText = "End acquisition"
Const $s_IdStartScanText = "Start scan"
Const $s_IdStopScanText = "Stop scan"
Const $s_IdBackToSetupText = "Back to setup"
Const $s_IdSaveText = "Ja"
Const $s_IdDontSaveText = "Nein"
; Constant numbers
Const $s_MinFreeSpaceC = 10*86.7
Const $s_MinFreeSpaceD = 10*(86.7+86.1)
Const $i_minBytesPerFile = 2500 * 1024 ;5000 * 1024
Const $i_NrOfRounds = 2
Const $i_WaitSec = 5
Const $i_MaxRepetitionTimesRadar = 6
Const $t_maxRepititionTime = 20*60*1000
; Strings
Const $s_LogError = "Could not open log-file."
Const $s_InternetConnectionActive = "You have an active connection"
Const $s_ServerToReach = "notesbi1.wsl.ch"
; Directories/Files
Const $s_DirK2 = "C:\K2FastWave\Mission\"
Const $s_GoogleDriveDir = "C:\Documents and Settings\fit-pc\My Documents\Google Drive\upGPR STR\"
; Paths to Programs
Const $s_FastWafe= "C:\K2FastWave\K2FastWave.exe"
Const $s_establishConnection='"C:\Documents and Settings\fit-pc\Desktop\Batch-Files\establishConnection.bat"'
Const $s_resetNumatoBat = "C:\Documents and Settings\fit-pc\Desktop\Batch-Files\ResetNumato.bat"
Const $s_SiRFLive = "C:\Program Files\SiRF\SiRFLive\Release\SiRFLive.exe"
; Constants for COM
Const $i_ComPortNumato = 10
; Strings for Numato
Const $s_NumatoAllInputs = "gpio iodir ff" & @CR
Const $s_NumatoRadarOn = "gpio set 0" & @CR
Const $s_NumatoRadarOff = "gpio clear 0" & @CR
Const $s_NumatoMotorUp = "gpio set 2" & @CR
Const $s_NumatoMotorDown = "gpio clear 2" & @CR
Const $s_NumatoAdcRead24V = "adc read 4" & @CR
Const $s_NumatoAdcRead12V = "adc read 5" & @CR
Const $i_NumatoMinTimeBetweenTwoCalls = 500
Const $s_NumatoDeviceID = "*VID_2A19*"

; RunWait("devcon disable " & $s_NumatoDeviceID)
; Sleep(5000)
;~  RunWait("devcon enable " & $s_NumatoDeviceID)
;~  Sleep(5000)
;~  $hPortNumato = _CommAPI_OpenCOMPort($i_ComPortNumato, 19200, 0, 8, 1)
;~  If @error Then
;~  	MsgBox(1,"","fail")
;~  EndIf
;~  MsgBox(1,"","success")		
;~  Exit

;BlockInput(1)
_PopupText("")
_WriteToLogFile("Start new measurement", 1)
WinMove($s_WinTask, "", 0, 0)

; ----------------------------------------------------
; Make radar measurement
; ----------------------------------------------------
	
$b_RadarMeasurementSuccessfull = False
$s_ErrorMessageForSMS = ""
$s_ErrorMessageForEmail = ""
$i_AttemptRadar = 0
$t_starttime = _Timer_Init()
$t_lastNumato = _Timer_Init()
Local $a_tooSmallFiles[1]
$hPortNumato = 0

Do
	; Actual date
    $s_YEAR = @YEAR 
    $s_MON = @MON 
    $s_DAY = @MDAY
    $s_HOUR = @HOUR
    $s_MIN = @MIN

    $cSplashWindow = SplashTextOn($s_SplashControlText, "", -1, -1, -1, -1, 4+16, "", 24)
	$i_AttemptRadar=$i_AttemptRadar+1
	_PopupText("Start new radar measurement, attempt " & $i_AttemptRadar)
	_WriteToLogFile("Start new radar measurement, attempt " & $i_AttemptRadar)
	
	; ----------------------------------------------------
	; check if Radar already in use
	; ----------------------------------------------------
	_WriteToLogFile("check if Radar already in use")
	If WinExists($s_WinMeas) And Not ControlCommand($s_WinMeas, $s_IdStartScanText, $i_IdStartScan, "IsEnabled", "") And (ControlCommand($s_WinMeas, $s_IdStopScanText, $i_IdStopScan, "IsEnabled", "") Or WinExists($s_WinSavePopup, $s_WinSavePopupText)) Then
		_WriteToLogFile("AutoIt tried to make a measurement. Radar already in use. Measurement abandoned.", 3)
	    _MailSMS("Radar already in use.")
		SplashOff()
		MsgBox( 0x1010, "Error", "AutoIt tried to make a measurement. Measurement abandoned.", 60)
		Exit
	EndIf
	 
	; ----------------------------------------------------
	; Set COM Port for Source and Lift
	; ----------------------------------------------------
	_WriteToLogFile("Set COM Port for Numato")
	;open com port
	$hPortNumato = _CommAPI_OpenCOMPort($i_ComPortNumato, 19200, 0, 8, 1)
	If @error Then
		If WinExists($s_WinResetNumatoCMD) Then
			_WriteToLogFile("Reset Numato was not successful.", 3)
			Exit
		Else
			_WriteToLogFile("Numato doesn't work. Restart it.", 3)
			_CommAPI_ClosePort($hPortNumato)
			Run($s_resetNumatoBat)
			Exit
		EndIf
		
		
;~ 		Sleep(5000)
;~ 		RunWait("devcon disable " & $s_NumatoDeviceID)
;~ 		Sleep(5000)
;~ 		RunWait("devcon enable " & $s_NumatoDeviceID)
;~ 		Sleep(5000)
;~ 		$hPortNumato = _CommAPI_OpenCOMPort($i_ComPortNumato, 19200, 0, 8, 1)
;~ 		If @error Then
;~ 			_ResetMeasurement("Failed to open port (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(), "OpenNumato")
;~ 			ContinueLoop
;~ 		 Else
;~ 			_WriteToLogFile("Numato works now.")
;~ 		EndIf
	EndIf
	$t_lastNumato = _Timer_Init()
	; Set digital outputs
	_NumatoSend($hPortNumato, $s_NumatoAllInputs)
	$i_errorZS = @error
	If $i_errorZS Then 
		_WriteToLogFile("Unable to set all numato gpio's to inputs (Error " & $i_errorZS & "): " & _WinAPI_GetLastErrorMessage(), 4)
		_AddMessageToQueue("Unable to set all numato gpio's to inputs (Error " & $i_errorZS & "): " & _WinAPI_GetLastErrorMessage(), "NumatoSetAllInput")
	EndIf
	
	_NumatoSend($hPortNumato, $s_NumatoRadarOff)
	If @error Then 
	   _ResetMeasurement("Unable to switch off Radar (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(), "NumatoSwitchRadarOff")
	   ContinueLoop
    EndIf
	_NumatoSend($hPortNumato, $s_NumatoMotorDown)
	If @error Then 
	   _ResetMeasurement("Unable to set Motor to 'Down' (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(), "NumatoMotorDown")
	   ContinueLoop
	EndIf
		
	;_NumatoReadVoltages()

	; ----------------------------------------------------
	; check if there is enough free space on the drives
	; ----------------------------------------------------
	_WriteToLogFile("check if enough free space on the drives")
	$FreeSpaceC = DriveSpaceFree ("C:\")
	If @error Then
		_ResetMeasurement("Drive C:\ is not available.","Drive C:\ not available")
	EndIf
	If $FreeSpaceC<$s_MinFreeSpaceC Then
		$spceMsg = "Free space on drive is low. There are only " & Round($FreeSpaceC,2) & "MB free space on C:"
		_AddMessageToQueue($spceMsg, "LowSpaceOnDrive")
		_WriteToLogFile($spceMsg, 3)
	Else
		_WriteToLogFile("Free space ok. There are " & Round($FreeSpaceC,2) & "MB free space on C:")
	EndIf
	
	; ----------------------------------------------------
	; Switch on power for radar
	; ----------------------------------------------------
	_WriteToLogFile("Switch on power for radar")
	_NumatoSend($hPortNumato, $s_NumatoRadarOn)
	If @error Then
		_ResetMeasurement("Unable to switch on Radar (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(),"NumatoRadarOn")
		ContinueLoop
    EndIf
	 
	Sleep(10000)
	
	_NumatoReadVoltages()

	; ----------------------------------------------------
	; Start K2FastWafe
	; ----------------------------------------------------
	$i_counter=0;
	Do
		$i_counter = $i_counter + 1
		_PopupText("Start K2FastWave, attempt " & $i_counter)
		_WriteToLogFile("Start K2FastWave, attempt " & $i_counter)
		; close K2 if it is already started
		If WinExists($s_WinK2) Then
			_WriteToLogFile("K2 already opened, close it first.", 4)
			WinClose($s_WinK2)
			WinWaitClose ($s_WinK2, "", $i_WaitSec)
		EndIf
		; start K2 
		_WriteToLogFile("Start K2.")
		Run($s_FastWafe)
		Sleep(15000)
		If Not ControlCommand($s_WinK2, $s_IdStartCalibrationText, $i_IdStartCalibration, "IsEnabled", "") Then
			_WriteToLogFile("Button 'Start Calibration' not enabled.",3)
			ContinueLoop
		EndIf
		WinActivate ( $s_WinK2 )
		
		; go to radar setup window
		_WriteToLogFile("Go to radar setup window")
		If WinExists($s_WinMeas) Then
			_WriteToLogFile("Window s_WinMeas exists. End acquisition.",4)
			ControlFocus ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )
			ControlClick ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )
			WinWaitActive($s_WinAcquisition, "", $i_WaitSec)
		EndIf
		If WinExists($s_WinAcquisition) Then
			_WriteToLogFile("Window " & $s_WinAcquisition & " exists.", 4)
			If WinExists($s_WinNewSurvey) Then
				_WriteToLogFile("Window $s_WinNewSurvey exists. Cancel it.",4)
				ControlFocus ($s_WinNewSurvey, "", $i_IdNewSurveyCancel )
				ControlClick ($s_WinNewSurvey, "", $i_IdNewSurveyCancel )
			EndIf
			_WriteToLogFile("Go back to Setup")
			ControlFocus ($s_WinAcquisition, $s_IdBackToSetupText, $i_IdBackToSetup )
			ControlClick ($s_WinAcquisition, $s_IdBackToSetupText, $i_IdBackToSetup )
			WinWaitActive($s_WinRadarSetup, "", $i_WaitSec)
		EndIf
		If Not WinExists($s_WinRadarSetup) Then
			_WriteToLogFile("Window Radar Setup is not displayed.", 3)
			ContinueLoop
		EndIf
		
		ControlFocus ($s_WinRadarSetup, $s_IdStartCalibrationText, $i_IdStartCalibration )
		ControlClick ($s_WinRadarSetup, $s_IdStartCalibrationText, $i_IdStartCalibration )
		WinWaitActive($s_WinAcquisition, "", 30)
		If Not WinExists($s_WinAcquisition) Then
			_WriteToLogFile("Window Acquisition is not displayed.", 3)
			ContinueLoop
		EndIf
	Until WinExists($s_WinAcquisition) Or ($i_counter>=3)
	
	If Not WinExists($s_WinAcquisition) Then
		_ResetMeasurement("Window Acquisition is also not displayed after 3 attempts.","Acquisition")
		ContinueLoop
	EndIf
	WinMove($s_WinAcquisition, "", 0, 137)
	
	; make new survey if necessary
	_WriteToLogFile("Make new survey if necessary")
	$s_ActSurvey = ControlCommand ($s_WinAcquisition,"",$i_IdCurrentSurvey,"GetCurrentSelection", "")
	$s_Survey = StringMid($s_YEAR,3,2) & $s_MON & $s_DAY
	If Not (StringMid($s_ActSurvey,10) = $s_Survey) Then
		_WriteToLogFile("Actual survey is not desired survey")
		$h_ComboboxSurvey = ControlGetHandle ( $s_WinAcquisition, "", $i_IdCurrentSurvey )
		$l_SurveyHistory = _GUICtrlComboBox_GetListArray($h_ComboboxSurvey)
		$i_ActSurveyInd = -1
		For $i = 1 To $l_SurveyHistory[0]
			If StringMid($l_SurveyHistory[$i],10) = $s_Survey Then
				$i_ActSurveyInd = $i - 1
				ExitLoop
			EndIf
		Next
		If $i_ActSurveyInd=-1 Then
			_WriteToLogFile("Desired survey does not already exists")
			ControlFocus ($s_WinAcquisition, $s_IdNewSurveyText, $i_IdNewSurvey )
			ControlClick ($s_WinAcquisition, $s_IdNewSurveyText, $i_IdNewSurvey )
			WinWaitActive($s_WinNewSurvey, "", $i_WaitSec)
			If Not WinExists($s_WinNewSurvey) Then
				_ResetMeasurement("Window New survey does not appear.","NewSurvey")
				ContinueLoop
			EndIf
			ControlFocus ($s_WinNewSurvey, "", $i_IdNewSurveyText )
			ControlSetText  ($s_WinNewSurvey, "", $i_IdNewSurveyText, $s_Survey)
			ControlFocus ($s_WinNewSurvey, "", $i_IdNewSurveyOK )
			If Not ControlCommand( $s_WinNewSurvey, "", $i_IdNewSurveyOK, "IsEnabled", "") Then
				_ResetMeasurement("Survey " & $s_Survey & " already exists.","SurveyExists")
				ContinueLoop
			EndIf
			ControlClick ($s_WinNewSurvey, "", $i_IdNewSurveyOK )
		Else
			_WriteToLogFile("Desired survey already exists. Select it", 4)
			ControlCommand( $s_WinAcquisition, "", $i_IdCurrentSurvey, "SetCurrentSelection", $i_ActSurveyInd)
		EndIf
	EndIf
	$s_ActSurvey = ControlCommand ($s_WinAcquisition,"",$i_IdCurrentSurvey,"GetCurrentSelection", "")
	If Not (StringMid($s_ActSurvey,10) = $s_Survey) Then
		_ResetMeasurement("Can not create new survey.","CreateNewSurvey")
		ContinueLoop
	EndIf
	; make new acquisition
	_WriteToLogFile("Make new acquisition")
; lino, das musst du entfernen
$s_Acquisition = $s_Survey & "_" & $s_HOUR & $s_MIN
ControlSetText( $s_WinAcquisition, "", $i_IdNewAcquisitionText, $s_Acquisition )
While (Not ControlCommand($s_WinAcquisition, $s_IdNewAcquisitionText, $i_IdNewAcquisition, "IsEnabled", ""))
 _WriteToLogFile("Acquisition " & $s_Acquisition & " already exist in survey " & $s_Survey,3)
 $s_MIN=$s_MIN+1
 $s_Acquisition = $s_Survey & "_" & $s_HOUR & $s_MIN
 ControlSetText( $s_WinAcquisition, "", $i_IdNewAcquisitionText, $s_Acquisition )
WEnd
_WriteToLogFile("Acquisition is " & $s_Acquisition)
;	$s_Acquisition = $s_Survey & "_" & $s_HOUR & $s_MIN
;	ControlSetText( $s_WinAcquisition, "", $i_IdNewAcquisitionText, $s_Acquisition ) 
;	If Not ControlCommand($s_WinAcquisition, $s_IdNewAcquisitionText, $i_IdNewAcquisition, "IsEnabled", "") Then
;		_ResetMeasurement("Acquisition " & $s_Acquisition & " already exist in survey " & $s_Survey,"Acquisition1")
;		ContinueLoop
;	EndIf
	ControlFocus ($s_WinAcquisition, $s_IdNewAcquisitionText, $i_IdNewAcquisition )
	ControlClick ($s_WinAcquisition, $s_IdNewAcquisitionText, $i_IdNewAcquisition )
	WinWaitActive($s_WinMeas, "", $i_WaitSec)

	If Not WinExists($s_WinMeas) Then
		_ResetMeasurement("Window new acquisition could not be displayed.","Acquisition2")
		ContinueLoop
	EndIf

	; ----------------------------------------------------
	; start scan
	; ----------------------------------------------------
	;_NumatoReadVoltages()
	_PopupText("Start scanning")
	_WriteToLogFile("Start scanning")
    $t_start10s = _Timer_Init()
	If Not WinExists($s_WinMeas) Then
		_ResetMeasurement("Measurement window does not exist.","MeasurementWindow")
		ContinueLoop
	EndIf
	ControlFocus ( $s_WinMeas, $s_IdStartScanText, $i_IdStartScan )
	ControlClick ( $s_WinMeas, $s_IdStartScanText, $i_IdStartScan )
	;_NumatoReadVoltages()
	Sleep(10000 - _Timer_Diff($t_start10s))
	$t_start10s = _Timer_Init()
	; ----------------------------------------------------
	; move radar up and down
	; ----------------------------------------------------
	For $j = 1 to $i_NrOfRounds
		; move radar up
		_PopupText("Round " & $j & ": Move radar up.")
		_WriteToLogFile("Round " & $j & ": Move radar up.")
		_NumatoSend($hPortNumato, $s_NumatoMotorUp)
		If @error Then
			_ResetMeasurement("Can not move up Lift (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(),"LiftcontrolUp")
			ContinueLoop 2
		 EndIf
		 Sleep(1000)
		_NumatoReadVoltages()
		Sleep(10000 - _Timer_Diff($t_start10s))
		$t_start10s = _Timer_Init()
		
		;wait
		_PopupText("Round " & $j & ": Wait on upper position.")
		_WriteToLogFile("Round " & $j & ": Wait on upper position.")
		;_NumatoReadVoltages()
		Sleep(10000 - _Timer_Diff($t_start10s))
		$t_start10s = _Timer_Init()

		; move radar down
		_PopupText("Round " & $j & ": Move radar down.")
		_WriteToLogFile("Round " & $j & ": Move radar down.")
		_NumatoSend($hPortNumato, $s_NumatoMotorDown)
		If @error Then
			_ResetMeasurement("Can not move down Lift (Error " & @error & "): " & _WinAPI_GetLastErrorMessage(),"LiftcontrolDown")
			ContinueLoop 2
		 EndIf
		 Sleep(1000)
	    ;_NumatoReadVoltages()
		Sleep(10000 - _Timer_Diff($t_start10s))
		$t_start10s = _Timer_Init()
		
		;wait
		_PopupText("Round " & $j & ": Wait on lower position.")
		_WriteToLogFile("Round " & $j & ": Wait on lower position.")
		;_NumatoReadVoltages()
		Sleep(10000 - _Timer_Diff($t_start10s))
		$t_start10s = _Timer_Init()
	Next

	; ----------------------------------------------------
	; stop scan
	; ----------------------------------------------------
	_PopupText("Stop scan and save")
	_WriteToLogFile("Stop scan")
	If Not WinExists($s_WinMeas) Then
		_ResetMeasurement("Measurement window does not exist.","MeasWin")
		ContinueLoop
	EndIf
	ControlFocus ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
	ControlClick ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )

	; ----------------------------------------------------
	; wait for save-popup and press yes
	; ----------------------------------------------------
	_WriteToLogFile("Save scan")
	WinWait($s_WinSavePopup, $s_WinSavePopupText, $i_WaitSec)
	If WinExists($s_WinSavePopup, $s_WinSavePopupText) Then
	    ControlClick ( $s_WinSavePopup, $s_IdSaveText, $i_IdSave )
	    ;ControlClick ( $s_WinSavePopup, $s_IdDontSaveText, $i_IdDontSave )
		
		; End acquisition
	    _WriteToLogFile("End acquisition")
	    ControlFocus ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )
	    ControlClick ($s_WinMeas, $s_IdEndAcquisitionText, $i_IdEndAcquisition )
	   
	    ; Close K2
	    _WriteToLogFile("Close K2")
	    WinClose($s_WinK2)
	Else
		_WriteToLogFile("Save window does not appear.", 3)
	    _AddMessageToQueue("Save window does not appear.","SaveWin")
    EndIf
	 
	; Kill K2 process if not already stopped
    ProcessWaitClose ( $s_ProcessK2, $i_WaitSec) 
    If ProcessExists($s_ProcessK2) Then 
		_WriteToLogFile("Process " & $s_ProcessK2 & " still exists. Kill Process", 3)
	    _AddMessageToQueue("Process " & $s_ProcessK2 & " still exists. Kill Process","KillProcess")
        ProcessClose ( $s_ProcessK2 )
        ProcessWaitClose ( $s_ProcessK2, $i_WaitSec) 
	    If ProcessExists($s_ProcessK2) Then 
		   _WriteToLogFile("I was not able to kill Process " & $s_ProcessK2 & ".", 3)
	       _AddMessageToQueue("I was not able to kill Process " & $s_ProcessK2 & ".","KillProcessFailed")
	    EndIf
    EndIf
	 
	 
	; ----------------------------------------------------
	; Check if *.dt - File existes and size > 2500 kB and copy to google drive
	; ----------------------------------------------------
	$s_DtFile = $s_DirK2 & $s_Survey & ".Mis\" & $s_Acquisition & ".ZON\LID10001.dt"
	If FileExists($s_DtFile) AND FileGetSize($s_DtFile)>$i_minBytesPerFile Then
        _WriteToLogFile("*.dt - File exists and its size is " & FileGetSize($s_DtFile)/1024 & "kB (Minimum Size: 2500 kB).")
;~ 		_WriteToLogFile("Copy file to google drive.")
;~ 		$i_fileCopyReturn = FileCopy($s_DtFile, $s_GoogleDriveDir & $s_Acquisition & ".dt", 8+0)
;~ 		If $i_fileCopyReturn=0 Then
;~ 		   _WriteToLogFile("Copying file to Google Drive failed.", 3)
;~ 	       _AddMessageToQueue("Copying file to Google Drive failed.","GoogleDrive")
;~ 		EndIf
    Else
	    If Not FileExists($s_DtFile) Then
		    _ResetMeasurement($s_DtFile & " does not exist.","DtFileDoesNotExist")
		    ContinueLoop
	    Else
		    _ResetMeasurement("The size of " & $s_DtFile & " is only " & FileGetSize($s_DtFile)/1024 & "kB (Minimum Size: 5000kB).","DtFile SizeTooSmall")
			_ArrayAdd($a_tooSmallFiles, $s_DtFile)
		    ContinueLoop
	    EndIf
    EndIf

	; ----------------------------------------------------
	; Switch off power for radar
	; ----------------------------------------------------
	_WriteToLogFile("Switch off power for Radar")
	_NumatoSend($hPortNumato, $s_NumatoRadarOff)
	$i_errorZS = @error
	If $i_errorZS Then
		_AddMessageToQueue("Can not switch off radar power (Error " & $i_errorZS & "): " & _WinAPI_GetLastErrorMessage() ,"SwitchOffRadar")
		_WriteToLogFile("Can not switch off radar power (Error " & $i_errorZS & "): " & _WinAPI_GetLastErrorMessage() , 3)
    EndIf
	;_NumatoReadVoltages()

	_PopupText("Radar Measurement successfully done")
	_WriteToLogFile("Radar Measurement successfully done")
	$b_RadarMeasurementSuccessfull = True
Until $b_RadarMeasurementSuccessfull Or (_Timer_Diff($t_starttime)>$t_maxRepititionTime) Or ($i_AttemptRadar > $i_MaxRepetitionTimesRadar)

; ----------------------------------------------------
; Delete all measurements which are smaller than 5000kB, if there is a good measurement
; ----------------------------------------------------
If $b_RadarMeasurementSuccessfull Then
    FOR $s_tooSmallFile IN $a_tooSmallFiles
	    If (Not($s_tooSmallFile = "")) AND FileExists($s_tooSmallFile) Then 
		   ;C:\K2FastWave\mission\140219.Mis\140219_1931.ZON\LID10001.dt
		   $s_tooSmallDirectory = StringTrimRight($s_tooSmallFile,12)
		   _AddMessageToQueue("Delete diretory " & $s_tooSmallDirectory & " whose *.dt-File is smaller than 5000kB.")
		   _WriteToLogFile("Delete directory " & $s_tooSmallDirectory & " whose *.dt-File is smaller than 5000kB.", 4)
		   DirRemove ( $s_tooSmallDirectory, 1 )
	    EndIf
    NEXT
 EndIf

; ----------------------------------------------------
; Close numato port
; ----------------------------------------------------
_WriteToLogFile("Close Port for Numato.")
_CommAPI_ClosePort($hPortNumato)
If @error Then
	_AddMessageToQueue("Can not close port for Numato: " & _WinAPI_GetLastErrorMessage() ,"CloseNumato")
	_WriteToLogFile("Can not close port for Numato: " & _WinAPI_GetLastErrorMessage() , 3)
EndIf

; ----------------------------------------------------
; send error messages
; ----------------------------------------------------
_WriteToLogFile("Check if there is a message to send over SMS or EMail.")
_PopupText("Check if there is a message to send over SMS or EMail.")
If ($b_RadarMeasurementSuccessfull) Then
    ; if both measurements successful, only send mail (no SMS)
	If (Not ($s_ErrorMessageForEmail = "")) Then
		_MailSMS("Radar measurements could be done successfully, but with errors:" & @CRLF & $s_ErrorMessageForEmail & @CRLF & @CRLF & "Attempts: " & $i_AttemptRadar & ", Time: " & _Timer_Diff($t_starttime)/1000/60 & " min.", 1, True, False, 0)
	EndIf
Else
	_MailSMS( "Radar Measurement could not be done successfully. Reason is:" & @CRLF & $s_ErrorMessageForSMS, 0, False, True, 0)

	_MailSMS( "Radar Measurement could not be done successfully. Reason is:" & @CRLF & $s_ErrorMessageForEmail & @CRLF & @CRLF & "Attempts: " & $i_AttemptRadar & ", Time: " & _Timer_Diff($t_starttime)/1000/60 & " min.", 0, True, False, 0)
EndIf

;BlockInput(0)
SplashOff()

_WriteToLogFile("End of program successfully reached.")

Exit
;============================================================================
; Function Name:   _NumatoSend($p_Port, $s_comm)

; Description:    Read something from Numato
; Parameters:     $p_Port 	 numatoport
;				  $s_comm    String to send to numato
;                 
; @error    on success-	     Received string
;			on failure-      error from _CommAPI_TransmitString
;===============================================================================
Func _NumatoSend($p_Port, $s_comm)
	;_WriteToLogFile("Numato: " & @SEC & "." & @MSEC)
	$i_sinceLastCall = _Timer_Diff($t_lastNumato)
	If $i_sinceLastCall<$i_NumatoMinTimeBetweenTwoCalls Then
		Sleep($i_NumatoMinTimeBetweenTwoCalls-$i_sinceLastCall)
	EndIf
	$s_ret=_CommAPI_TransmitString($p_Port, $s_comm)
	$i_errorZS = @error
	$t_lastNumato = _Timer_Init()
	If $i_errorZS Then
	   SetError($i_errorZS)
	EndIf
	Return $s_ret
EndFunc   ;==>_NumatoSend

;============================================================================
; Function Name:   _NumatoRead($s_command)

; Description:    Read something from Numato
; Parameters:     $s_command command for Numato
;                 
; Returns:  on success-	     Received string
; @error    on success-	     Received string
;			on failure-      1 unable to use the DLL file
;                            2 unknown "return type"
;                            3 "function" not found in the DLL file
;                            4 bad number of parameters
;                            5 bad parameter
;							 6 unable to transmit string
;                           -1 function fails (To get extended error information, call _WinAPI_GetLastError or _WinAPI_GetLastErrorMessage)
;                           -2 timeout
;                           -3 error in BinaryToString
;===============================================================================
Func _NumatoRead($s_command)
	$s_ret = _NumatoSend($hPortNumato, $s_command)
    $i_errorBuffer=@error
	If $i_errorBuffer Then 
		;_WriteToLogFile("Unable to send string to numato: (Error " & @error & "; " & _WinAPI_GetLastErrorMessage() & ")", 3)
		SetError(6)
		Return $s_ret
	EndIf
	$s_ret=_CommAPI_ReceiveString($hPortNumato, 1000)
    $i_errorBuffer=@error
	If $i_errorBuffer Then 
		;_WriteToLogFile("Unable to read from Numato: (" & $i_errorBuffer & ") " & _WinAPI_GetLastErrorMessage(), 3)
		SetError($i_errorBuffer)
		Return $s_ret
	EndIf
	$a_ret = StringSplit ( $s_ret, @LF & @CR, 1)
	return $a_ret[$a_ret[0]-1]
 EndFunc   ;==>_NumatoRead
 
 
;============================================================================
; Function Name:   _NumatoReadVoltages()

; Description:    Read Voltages using Numato
;                 
;===============================================================================
Func _NumatoReadVoltages()
	$s_voltageAlarm = False
    $s_ret24 = _NumatoRead($s_NumatoAdcRead24V)
	$i_errorBuffer = @error
	If $i_errorBuffer Then 
		_WriteToLogFile("Unable to read adc for 24V (Error " & $i_errorBuffer & "): " & _WinAPI_GetLastErrorMessage() & ".", 3)
		_AddMessageToQueue("Unable to read adc for 24V (Error " & $i_errorBuffer & "): " & _WinAPI_GetLastErrorMessage() & ".", "Read24V")
		$s_ret24 = "NaN"
	 Else
		$s_ret24 = $s_ret24*5/1023/10*60
		If $s_ret24 < 23 Then
		   $s_voltageAlarm = True
		EndIf
	EndIf
    $s_ret12 = _NumatoRead($s_NumatoAdcRead12V)
	If @error Then 
		_WriteToLogFile("Unable to read adc for 12V (Error " & $i_errorBuffer & "): " & _WinAPI_GetLastErrorMessage() & ".", 3)
		_AddMessageToQueue("Unable to read adc for 12V (Error " & $i_errorBuffer & "): " & _WinAPI_GetLastErrorMessage() & ".", "Read12V")
		$s_ret12 = "NaN"
	 Else
		$s_ret12 = $s_ret12*5/1023/10*30
		If $s_ret12 < 11.5 Then
		   $s_voltageAlarm = True
		EndIf
    EndIf
    if $s_voltageAlarm Then
	   _WriteToLogFile("Voltages: " & $s_ret24 & " V (24 V) and " & $s_ret12 & " V (12 V).", 4)
    Else
	   ;_WriteToLogFile($s_ret24 & ", " & $s_ret12, 6)
    EndIf
	
	;write to Energy-File
	$file = FileOpen("Energy.csv", 1)
	If $file = -1 Then
	   _WriteToLogFile("Unable to write to Energy.csv.", 3)
	   _AddMessageToQueue("Unable to write to Energy.csv.", "Energy.csv")
	EndIf
	$s_timestamp = @MDAY & "." & @MON & "." & @YEAR & " " & @Hour & ":" & @Min & ":" & @SEC
	FileWriteLine($file, $s_timestamp & ", " & $s_ret12 & ", " & $s_ret24)
	FileClose($file)
 EndFunc   ;==>_NumatoReadVoltages

;===============================================================================
; Function			_WriteToLogFile($s_inputStr)
;  
; Description:		Write something to logfile
;
; Parameter(s):		$s_inputStr: String to write to logfile
;					$i_mode:	1: as a title
;								2: normalLogEntry
;								3: error
;								4: unplanned incident (unexpected, but program can solve problem and continue)
;								5: some ultrasonic measurements are of bad quality
;								default: 2
;								6: voltage
; 
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _WriteToLogFile($s_inputStr, $i_mode=2)
	$file = FileOpen("AutoitLog.txt", 1)
	; Check If file opened for writing
	If $file = -1 Then
		_ResetMeasurement($s_LogError,"WriteToAutoIt")
	EndIf

	$s_inputStr = StringReplace($s_inputStr,@CRLF," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@CR," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@LF," ./. ")
	$s_timestamp = @MDAY & "." & @MON & "." & @YEAR & " " & @Hour & ":" & @Min & ":" & @SEC
	Switch $i_mode 
		Case 1
			$s_inputStr = @CRLF & "  ++++++++++++++++++++++++++++++++++++++" & @CRLF & "  + " & $s_timestamp & @CRLF & "  + " & $s_inputStr & @CRLF & "  ++++++++++++++++++++++++++++++++++++++"
		Case 2
			$s_inputStr = "[" & $s_timestamp & " M] " & $s_inputStr
		Case 3
			$s_inputStr = "[" & $s_timestamp & " E] " & "E R R O R : " & $s_inputStr
		Case 4
			$s_inputStr = "[" & $s_timestamp & " U] " & "UNPLANNED INCIDENT: " & $s_inputStr
		Case 5
			$s_inputStr = "[" & $s_timestamp & " B] " & "BAD QUALITY: " & $s_inputStr
		Case 6
			$s_inputStr = "[" & $s_timestamp & " V] " & $s_inputStr
	EndSwitch
	
	FileWriteLine($file, $s_inputStr)
	FileClose($file)
EndFunc

;===============================================================================
; Function			_PopupText($s_ActualAction)
;  
; Description:		Update Text to inform user that and which measurement is
;					in process
;
; Parameter(s):		$s_ActualAction: Action that is carried out at the moment
; 
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _PopupText($s_ActualAction)
	$s_Message = "A measurement is in progress. Please do not use mouse or keyboard." & @CRLF & @CRLF & "Actual action: " & @CRLF & $s_ActualAction
	ControlSetText($s_SplashControlText, "", "Static1", $s_Message)
EndFunc

;===============================================================================
; Function			_RunAndWaitForAnswer($s_Command)
;  
; Description:		Runs a program, wait for the answer and return it
;
; Parameter(s):		$s_Command: Program to Run
;
; Returns:			answer
; 
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _RunAndWaitForAnswer($s_Command)   
	$cmdreturn = ""   
	Local $stream = Run($s_command, @SystemDir, @SW_HIDE, $STDOUT_CHILD+$STDIN_CHILD) 
	Local $line     
	While 1         
		$line = StdoutRead($stream)         
		If @error Then 
			ExitLoop    
		EndIf		
		$cmdreturn &= $line     
	WEnd     
	StdinWrite($stream,"")     
	StdioClose($stream)     
	Return $cmdreturn 
EndFunc   ;==>_RunAndWaitForAnswer

;===============================================================================
; Function        _AddMessageToQueue([$s_MessageMail [, $s_MessageSMS]])
;  
; Description:		Add To Message queue. At the end, an Email or SMS is send.
;					If $s_MessageMail not "", the message is added to Mail-queue.
;					If $s_MessageSMS not "", he message is added to Mail-queue.
;
; Parameter(s):		$s_MessageMail:	Message to be added to Mail queue
;										Default: ""
;             		$s_MessageSMS:	Message to be added to SMS queue
;										Default: ""
; 
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _AddMessageToQueue($s_MessageMail = "", $s_MessageSMS = "")
	If Not $s_MessageMail="" Then
		$s_ErrorMessageForEmail = $s_ErrorMessageForEmail & $s_MessageMail & @CRLF
	EndIf
	If Not $s_MessageSMS="" Then
		$s_ErrorMessageForSMS = $s_ErrorMessageForSMS & $s_MessageSMS & ", "
	EndIf
EndFunc ;==>_AddMessageToQueue

;===============================================================================
; Function        _MailSMS($s_ErrorMessage [,$i_Type, [$b_SendMail, [$b_SendSMS ]]])
;  
; Description:		Sends a Email or SMS. 
;					Please use this Function and not directly _SendMailAndSMS
;
; Parameter(s):		$s_ErrorMessage: Error message
;					$i_Type: 		0: Error message
;									1: Information message
;									2: success message
;									3: Other message
;									default: 0
;					$b_SendMail:	True: Send Mail
;									False: Do not send Mail
;									Default: True
;					$b_SendSMS:		True: Send SMS
;									False: Do not send SMS
;									Default: True
;					$i_Receiver:	0: Lino Schmid
;									1: Georg Brunnhofer
;									2: Achim Heilig
;									Default: 0
; 
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _MailSMS($s_ErrorMessage, $i_Type=0, $b_SendMail = True, $b_SendSMS = True, $i_Receiver=0)
   If $b_SendMail And $b_SendSMS Then
	  _WriteToLogFile("Send Mail and SMS...")
   ElseIf $b_SendMail Then
	  _WriteToLogFile("Send Mail...")
   ElseIf $b_SendSMS Then
	  _WriteToLogFile("Send SMS...")
   Else
	  _WriteToLogFile("No Mail and no SMS to be sent",3)
	  Return
   EndIf
   
   If ((@HOUR>=00 AND @HOUR<=03) OR (@HOUR>08 AND @HOUR<14)) Then
	  ;establish internet connection
	  _PopupText("Establish internet connection to send an Email or SMS")
	  _WriteToLogFile("Establish internet connection")
	  $s_answerEC = _RunAndWaitForAnswer($s_establishConnection & " " & $s_ServerToReach)
	  $a_answerEC = StringSplit($s_answerEC,@CR & @LF,1)
	  If $a_answerEC[0]<3 Then
		 _WriteToLogFile("Can not parse response from " & $s_establishConnection & ". Response was: " & $s_answerEC,3)
	  ElseIf StringCompare($a_answerEC[$a_answerEC[0]-1],$s_InternetConnectionActive)=0 Then
		 _WriteToLogFile("Internet connection active.")
	  ElseIf Not (StringInStr($a_answerEC[$a_answerEC[0]-1],$s_InternetConnectionActive)=0) Then  
		 _WriteToLogFile("Internet connection was not active, but is now. Reason: " & $a_answerEC[$a_answerEC[0]-1], 4)
	  Else
		 _WriteToLogFile("Could not establish connection.",3)
	  EndIf
   EndIf
   
   ;send mail or sms
   _PopupText("Send Email or SMS")
   $s_MailSmsError = _SendMailAndSMS($s_Station, $s_ErrorMessage, $i_Type, $b_SendMail, $b_SendSMS, $i_Receiver)
   $i_errorCode = @error
   If Not (BitAND($i_errorCode,7) = 0) Then
	  If ((@HOUR>=00 AND @HOUR<=03) OR (@HOUR>08 AND @HOUR<14)) Then
		 _WriteToLogFile("Could not send EMail or SMS." & @CRLF & $s_MailSmsError, 3)
	  Else
		 _WriteToLogFile("Could not send EMail or SMS, but that's ok because the WLAN-Connection is switched off.")
	  EndIf
   ElseIf (BitAND($i_errorCode,8) = 8) Then
	  _WriteToLogFile("Transmission was successful, but " & $s_MailSmsError)
   Else
	  _WriteToLogFile("Transmission was successful")
   EndIf
EndFunc ;==>_MailSMS

;===============================================================================
; Function        _ResetMeasurement([$s_ErrorMessage [, $s_ErrorMessageSMS]])
;  
; Description:		This method can be called if an error occurs.
;					- A running Measurement will be stoped
;					- Everything will be reset
;					- a MessageBox will be shown ($s_ErrorMessage)
;					- an error-entry will be done in Log-File ($s_ErrorMessage)
;					- a message will be added to email queue ($s_ErrorMessage).
;					- a message will be added to SMS queue ($s_ErrorMessageSMS).
;
; Parameter(s):		$s_ErrorMessage:	Error-message or "" if no MessageBox, no entry in Log-File and no message over E-Mail
;										is desired
;										Default: ""
;             		$s_ErrorMessageSMS:	Error-message or "" if no advices over SMS
;										is desired
;										Default: ""
; 
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _ResetMeasurement($s_ErrorMessage = "", $s_ErrorMessageSMS = "")
	_AddMessageToQueue($s_ErrorMessage, $s_ErrorMessageSMS)
	; log error
	If Not ($s_ErrorMessage = $s_LogError) Then
		 _WriteToLogFile($s_ErrorMessage & " - Program can not handle this error and therefore resets and restarts measurement.", 3)
    EndIf
	$s_executedSteps = "Executed steps during ResetMeasurement: "  
	; stop scan
	If WinExists($s_WinMeas) And ControlCommand($s_WinMeas, $s_IdStopScanText, $i_IdStopScan, "IsEnabled", "") Then
	    $s_executedSteps = $s_executedSteps & " Stop Scan, "
		ControlFocus ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
		ControlClick ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
		; wait for save-popup and press no
		WinWait($s_WinSavePopup, $s_WinSavePopupText, $i_WaitSec)
		If WinExists($s_WinSavePopup, $s_WinSavePopupText) Then
		    $s_executedSteps = $s_executedSteps & " Dont save scan, "
			ControlClick ( $s_WinSavePopup, $s_IdDontSaveText, $i_IdDontSave )
		EndIf
	EndIf
	; Close K2
	$s_executedSteps = $s_executedSteps & " Close K2, "
	WinClose($s_WinK2)
    ; stop K2 process
	ProcessWaitClose ( $s_ProcessK2, $i_WaitSec) 
    If ProcessExists($s_ProcessK2) Then 
	    $s_executedSteps = $s_executedSteps & " Kill Process K2FastWave.exe, "
		ProcessClose ( $s_ProcessK2 )
	    ProcessWaitClose ( $s_ProcessK2, $i_WaitSec)
		If ProcessExists($s_ProcessK2) Then 
	        $s_executedSteps = $s_executedSteps & " Can not Kill K2FastWave.exe, "
	    EndIf
    EndIf
	;Close SiRFLive
	;$s_executedSteps = $s_executedSteps & " Close SiRFLive, "
	;WinClose($s_WinSiRFLive)
	; Close COM
	If Not ($hPortNumato = 0) Then
	    $s_executedSteps = $s_executedSteps & " Close Numato, "
		_CommAPI_ClosePort($hPortNumato)
		$hPortNumato = 0
	EndIf
	; show error message
	If Not $s_ErrorMessage="" Then
	    $s_executedSteps = $s_executedSteps & " Show ErrorMessage, "
		SplashOff()
		MsgBox( 0x1010, "Error", "AutoIt resets Measurement." & @CRLF & @CRLF & $s_ErrorMessage, 3*$i_WaitSec)
    EndIf
    If Not ($s_ErrorMessage = $s_LogError) Then
		 _WriteToLogFile(StringTrimRight ( $s_executedSteps, 2 ) & ".")
    EndIf
EndFunc 
