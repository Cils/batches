TITLE ResetNumatoCMD
@echo off
echo start resetNumato
echo ------------------------------ >> taskLog.txt
echo start resetNumato on %date% at %time% >> taskLog.txt
echo ------------------------------ >> taskLog.txt
timeout /T 10  /NOBREAK

echo disable now
echo disable now >> taskLog.txt

devcon disable *VID_2A19* >> taskLog.txt
timeout /T 10  /NOBREAK
echo enable now
echo enable now >> taskLog.txt
devcon enable *VID_2A19* >> taskLog.txt
timeout /T 10  /NOBREAK

echo Start Autoit >> taskLog.txt
echo Start Autoit
"C:\batches\PRM_Wfj_v2.exe"
echo AutoIt finished at %time% >> taskLog.txt
echo resetNumato finished at %time% >> taskLog.txt
echo " " >> taskLog.txt