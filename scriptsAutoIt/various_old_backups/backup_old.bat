REM --- Copy files to phoenix ---

robocopy "D:\scans\2019-20" "X:\Radar\Radardata\2019-20\upGPR WFJ" /s /maxage:30 /copy:DT /dcopy:DT /r:2 /w:10 /log+:"D:\scans\2019-20\RobocopyLog.log"  /ns /fp /np /ndl

@echo off

IF %ERRORLEVEL% gtr 7 (
	set msg = "Copy of upGPR data transfer was unsuccessfully. Robocopy error!"
	C:\batches\SendMail.exe "Copy of upGPR data transfer was unsuccessfully. Robocopy error!" 2
	echo %msg%
)

echo "Robocopy output:"
echo %ERRORLEVEL% 

REM ---Copy Logfile Autoit ---
robocopy "C:\batches" "X:\Radar\Radardata\2019-20\upGPR WFJ" AutoitLog.txt  /copy:DT
robocopy "D:\scans\2019-20" "X:\Radar\Radardata\2019-20\upGPR WFJ" RobocopyLog.log  /copy:DT

@echo on

REM pause
