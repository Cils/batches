

;===============================================================================
; Function          _SendMail($s_Station, $s_ErrorMessage [,$i_Type, $i_Receiver,$i_Sender])
;
; Description:		Sends an Email inform about errors
;
; Parameter(s):		$s_Statione: measurement station sending error
;					$s_ErrorMessage: Error message
;					$i_Type: 		0: Error message
;									1: Information message
;									2: success message
;									3: Other message
;									default: 0
;					$i_Receiver:	0: Achille Capelli
;									1: Alec van Herwijnen
;									or string with email
;									Default: 0
;					$i_Receiver:	0: Achille Capelli
;									1: Alec van Herwijnen
;									or string with email
;									Default: 0
;
; Return Value(s):	Success:		1
;					Failure:		Return Message and set @error
;					@error:			Binary x  x  
;									       |  + 1 Sending Mail failed
;									       + 4 Wrong receiver
;									       
;
#cs
usage:
$s_ErrorMessage = _SendMail($s_Station, $s_SMSMessage, 0, 1,"example@slf.ch")
$i_errorCode = @error
If (BitAND($i_errorCode,1) = 1) Then
   $b_errorMail=True
EndIf
If (BitAND($i_errorCode,2) = 2) Then
   $b_errorWrongReceiver=True
EndIf
#ce

; Author:			Achille Capelli, SLF, 2019
; Source from:		Lino Schmid, SLF Davos			
;===============================================================================
Func _SendMail($s_Station, $s_ErrorMessage, $i_Type=0, $i_Receiver=0, $i_Sender=0)
	Const $s_SmtpServer = "notesbi1.wsl.ch"
	Const $s_FromName = "AutoIt"
    Const $s_ToMailAddressAC = "Achille.Capelli@slf.ch"
	Const $s_ToMailAddressLino = "Lino.Schmid@slf.ch"
	Const $s_ToMailAddressAlec = "vanherwijnen@slf.ch"
	Const $s_AttachFiles = ""
	Const $s_CcAddress = ""
	Const $s_BccAddress = ""
	Const $s_Importance="Normal"
	Const $s_Username = ""
	Const $s_Password = ""
	Const $IPPort = 25
	Const $ssl = 0

	Const $const_sendMailFailed = 1
	Const $const_wrongReceiver = 2


	$i_errorCode=0
	$s_MailError=""

	If $i_Type=0 Then
		$s_Subject = "Errormessage from " & $s_Station
	ElseIf $i_Type=1 Then
		$s_Subject = "Information from " & $s_Station
	ElseIf $i_Type=2 Then
		$s_Subject = "Success message from " & $s_Station
	Else
		$s_Subject = "Message from " & $s_Station
	EndIf
	
	
	If IsString( $i_Sender) Then
		$s_FromAddress =	$i_Sender
	ElseIf $i_Sender=0 Then
		$s_FromAddress = $s_ToMailAddressAC
	ElseIf $i_Sender=1 Then
		$s_FromAddress = $s_ToMailAddressAlec
	Else
		$i_errorCode = BitOR($i_errorCode, $const_sendMailFailed +  $const_wrongReceiver)
		SetError($i_errorCode)
		Return "The sender " & $i_Sender & " is unknown."
	EndIf
	
	If IsString( $i_Receiver) Then
		$s_ToMailAddress =	$i_Receiver
	ElseIf $i_Receiver=0 Then
		$s_ToMailAddress = $s_ToMailAddressAC
    ElseIf $i_Receiver=1 Then
		$s_ToMailAddress = $s_ToMailAddressAlec
	ElseIf $i_Receiver=2 Then
	    SetError(4)
		Return "The receiver Lino Schmid should not anymore be disturbed."
	ElseIf IsString( $i_Receiver) Then
		$s_ToMailAddress =	$i_Receiver
	Else
		$i_errorCode = BitOR($i_errorCode, $const_sendMailFailed + $const_wrongReceiver)
		SetError($i_errorCode)
		Return "The receiver " & $i_Receiver & " is unknown."
	EndIf

	$as_Body = $s_ErrorMessage

	; Send Mail
	
	$s_MailSmtpError = _SmtpMail($s_SmtpServer, $s_FromName, $s_FromAddress, $s_ToMailAddress, $s_Subject, $as_Body, $s_AttachFiles, $s_CcAddress, $s_BccAddress, $s_Importance, $s_Username, $s_Password, $IPPort, $ssl)
	If @error Then
	   $i_errorCode = BitOR($i_errorCode, $const_sendMailFailed)
	   $s_MailError = $s_MailSmtpError
	EndIf
	


	If $i_errorCode=0 Then
	   Return 1
	Else
	   $s_errorString = ""
	   If Not (StringCompare($s_MailError,"")=0) Then
		  $s_errorString = "Mail error: " & @CRLF & $s_MailError
	   EndIf
	   SetError($i_errorCode)
	   Return $s_errorString
	EndIf
EndFunc


;===============================================================================
; Function          _SendMailAndSMS($s_Station, $s_ErrorMessage [,$i_Type, [$b_SendMail, [$b_SendSMS ]]])
;
; Description:		Sends an Email to achille.capelli@slf.ch
;					and an SMS to 0798450476
;					to inform about errors
;
; Parameter(s):		$s_Statione: measurement station sending error
;					$s_ErrorMessage: Error message
;					$i_Type: 		0: Error message
;									1: Information message
;									2: success message
;									3: Other message
;									default: 0
;					$b_SendMail:	True: Send Mail
;									False: Do not send Mail
;									Default: True
;					$b_SendSMS:		True: Send SMS
;									False: Do not send SMS
;									Default: True
;					$i_Receiver:	0: Achille Capelli
;									1: Georg Brunnhofer
;									2: Achim Heilig
;									3: Lino Schmid
;									Default: 0
;
; Return Value(s):	Success:		1
;					Failure:		Return Message and set @error
;					@error:			Binary x  x  x  x
;									       |  |  |  + 1 Sending Mail failed
;									       |  |  + 2 Sending SMS failed
;									       |  + 4 Wrong receiver
;									       + 8 SMS Truncated
;
#cs
usage:
$s_ErrorMessage = _SendMailAndSMS($s_Station, $s_SMSMessage, 0, True, True)
$i_errorCode = @error
If (BitAND($i_errorCode,1) = 1) Then
   $b_errorMail=True
EndIf
If (BitAND($i_errorCode,2) = 2) Then
   $b_errorSMS=True
EndIf
If (BitAND($i_errorCode,4) = 4) Then
   $b_errorWrongReceiver=True
EndIf
If (BitAND($i_errorCode,8) = 8) Then
   $b_smsTruncated=True
EndIf
#ce
; Author:			Lino Schmid, SLF Davos
; Edited:			Achille Capelli, SLF, 2018
;===============================================================================
Func _SendMailAndSMS($s_Station, $s_ErrorMessage, $i_Type=0, $b_SendMail = True, $b_SendSMS = False, $i_Receiver=0)
	Const $s_SmtpServer = "notesbi1.wsl.ch"
	Const $s_FromName = "AutoIt"
	Const $s_FromAddress = "Achille.Capelli@slf.ch"
    Const $s_ToMailAddressAC = "Achille.Capelli@slf.ch"
	Const $s_ToSMSAddressAC = "0041798450476@sms.switch.ch"
	Const $s_ToMailAddressLino = "Lino.Schmid@slf.ch"
	Const $s_ToSMSAddressLino = "0041793590516@sms.switch.ch"
	Const $s_ToMailAddressGeorg = ""
	Const $s_ToSMSAddressGeorg = ""
	Const $s_ToMailAddressAchim = "heilig@r-hm.de"
	Const $s_ToSMSAddressAchim = "00491777825860@sms.switch.ch"
	Const $s_AttachFiles = ""
	Const $s_CcAddress = ""
	Const $s_BccAddress = ""
	Const $s_Importance="Normal"
	Const $s_Username = ""
	Const $s_Password = ""
	Const $IPPort = 25
	Const $ssl = 0

	Const $const_sendMailFailed = 1
	Const $const_sendSMSFailed = 2
	Const $const_wrongReceiver = 4
	Const $const_SMSTruncated = 8

	$i_errorCode=0
	$s_MailError=""
	$s_SMSError=""

	If $i_Type=0 Then
		$s_Subject = "Errormessage from " & $s_Station
	ElseIf $i_Type=1 Then
		$s_Subject = "Information from " & $s_Station
	ElseIf $i_Type=2 Then
		$s_Subject = "Success message from " & $s_Station
	Else
		$s_Subject = "Message from " & $s_Station
	EndIf

	If $i_Receiver=0 Then
		$s_ToMailAddress = $s_ToMailAddressAC
		$s_ToSMSAddress = $s_ToSMSAddressAC
    ElseIf $i_Receiver=3 Then
	    SetError(4)
		Return "The receiver Georg Brunnhofer should not anymore be disturbed."
	ElseIf $i_Receiver=1 Then
		SetError(4)
		Return "The receiver Georg Brunnhofer should not anymore be disturbed."
	ElseIf $i_Receiver=2 Then
		$s_ToMailAddress = $s_ToMailAddressAchim
		$s_ToSMSAddress = $s_ToSMSAddressAchim
	 Else
		$i_errorCode = BitOR($i_errorCode, $const_sendMailFailed + $const_sendSMSFailed + $const_wrongReceiver)
		SetError($i_errorCode)
		Return "The receiver " & $i_Receiver & " is unknown."
	EndIf

	$as_Body = $s_ErrorMessage

	; Send Mail
	If $b_SendMail Then
		$s_MailSmtpError = _SmtpMail($s_SmtpServer, $s_FromName, $s_FromAddress, $s_ToMailAddress, $s_Subject, $as_Body, $s_AttachFiles, $s_CcAddress, $s_BccAddress, $s_Importance, $s_Username, $s_Password, $IPPort, $ssl)
		If @error Then
		   $i_errorCode = BitOR($i_errorCode, $const_sendMailFailed)
		   $s_MailError = $s_MailSmtpError
		EndIf
	EndIf

	; Send SMS
	If $b_SendSMS Then
		If StringLen($s_ErrorMessage)>160 Then
		   $i_errorCode = BitOR($i_errorCode, $const_SMSTruncated)
		   $as_Body = StringLeft($s_ErrorMessage,160-22) & ".. [Message truncated]"
		EndIf
		$s_SMSSmtpError = _SmtpMail($s_SmtpServer, $s_FromName, $s_FromAddress, $s_ToSMSAddress, $s_Subject, $as_Body, $s_AttachFiles, $s_CcAddress, $s_BccAddress, $s_Importance, $s_Username, $s_Password, $IPPort, $ssl)
		If @error Then
			$i_errorCode = BitOR($i_errorCode, $const_sendSMSFailed)
			$s_SMSError = $s_SMSSmtpError
		EndIf
	EndIf

	If $i_errorCode=0 Then
	   Return 1
	Else
	   $s_errorString = ""
	   If Not (StringCompare($s_MailError,"")=0) Then
		  $s_errorString = "Mail error: " & @CRLF & $s_MailError
	   EndIf
	   If Not (StringCompare($s_SMSError,"")=0) Then
		  If Not (StringCompare($s_errorString,"")=0) Then
			 $s_errorString = $s_errorString & @CRLF & @CRLF
		  EndIf
		  $s_errorString = $s_errorString & "SMS error: " & @CRLF & $s_SMSError
	   EndIf
	   If (BitAND($i_errorCode, $const_SMSTruncated) = $const_SMSTruncated) Then
		  If Not (StringCompare($s_errorString,"")=0) Then
			 $s_errorString = $s_errorString & @CRLF & @CRLF
		  EndIf
		  $s_errorString = $s_errorString & "Message length was " & StringLen($s_ErrorMessage) & ". Truncated to 160 characters."
	   EndIf
	   SetError($i_errorCode)
	   Return $s_errorString
	EndIf
EndFunc

;===============================================================================
; Function 			_SmtpMail($s_SmtpServer, $s_FromName, $s_FromAddress, $s_ToAddress, $s_Subject, $as_Body[, $s_AttachFiles[, $s_CcAddress[, $s_BccAddress[, $s_Importance[, $s_Username[, $s_Password[, $IPPort[, $ssl]]]]]]]])
;
; Description:		Sends an Email over smtp
;
; Parameter(s):		$s_SmtpServer:	Name of smtp-server (ex. "notesbi1.wsl.ch")
;					$s_FromName: 	Name of sender (ex. Lino Schmid)
;					$s_FromAddress:	Email of sender (ex. Lino.Schmid@slf.ch)
;					$s_ToAddress: 	Email of recipient (ex. Lino.Schmid@slf.ch)
;					$s_Subject: 	Subject of Email
;					$as_Body: 		Body of Email
;					$s_AttachFiles: Path to file to attach or ""
;									default: ""
;					$s_CcAddress: 	Email of copy recipient (ex. Lino.Schmid@slf.ch) or ""
;									default: ""
;					$s_BccAddress:	Email of blind copy recipient (ex. Lino.Schmid@slf.ch) or ""
;									default: ""
;					$s_Importance:  Importance ("High", "Normal" or "Low")
;									default: "Normal"
;					$s_Username:	Username to send email or ""
;									default: ""
;					$s_Password: 	Passwort to send email or ""
;									default: ""
;					$IPPort: 		smtp server port
;									default: 25
;					$ssl: 			if "0" dont use ssl, otherwise use ssl
;									default: 0
;
; Return Value(s):	Success:		1
;					Failure:		Return Error Message and sets @error to 1
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _SmtpMail($s_SmtpServer, $s_FromName, $s_FromAddress, $s_ToAddress, $s_Subject, $as_Body, $s_AttachFiles = "", $s_CcAddress = "", $s_BccAddress = "", $s_Importance="Normal", $s_Username = "", $s_Password = "", $IPPort = 25, $ssl = 0)

	Local $objEmail = ObjCreate("CDO.Message")
	if(@error) Then
		SetError(1)
		Return "Reference to COM object CDO.Message could not be create."
	EndIf

	Global $o_ComErrorDescription=""
	Global $o_ComError = ObjEvent("AutoIt.Error", "_ComErrFunc")    ; Initialize a COM error handler

	$objEmail.From = '"' & $s_FromName & '" <' & $s_FromAddress & '>'
	$objEmail.To = $s_ToAddress
	Local $i_Error = 0
	Local $i_Error_desciption = ""
	If $s_CcAddress <> "" Then $objEmail.Cc = $s_CcAddress
	If $s_BccAddress <> "" Then $objEmail.Bcc = $s_BccAddress
	$objEmail.Subject = $s_Subject

	If StringInStr($as_Body, "<") And StringInStr($as_Body, ">") Then
		$objEmail.HTMLBody = $as_Body
	Else
		$objEmail.Textbody = $as_Body & @CRLF
	EndIf

	If $s_AttachFiles <> "" Then
		Local $S_Files2Attach = StringSplit($s_AttachFiles, ";")
		For $x = 1 To $S_Files2Attach[0]
			$S_Files2Attach[$x] = _PathFull($S_Files2Attach[$x])
			ConsoleWrite('@@ Debug(62) : $S_Files2Attach = ' & $S_Files2Attach & @LF & '>Error code: ' & @error & @LF) ;### Debug Console
			If FileExists($S_Files2Attach[$x]) Then
				$objEmail.AddAttachment ($S_Files2Attach[$x])
			Else
				ConsoleWrite('!> File not found to attach: ' & $S_Files2Attach[$x] & @LF)
				SetError(1)
				Return "File not found to attach: " & $S_Files2Attach[$x]
			EndIf
		Next
	EndIf

	$objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
	$objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = $s_SmtpServer

	If Number($IPPort) = 0 then $IPPort = 25

	$objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = $IPPort

	;Authenticated SMTP
	If $s_Username <> "" Then
		$objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1
		$objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusername") = $s_Username
		$objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendpassword") = $s_Password
	EndIf
	If $ssl Then
		$objEmail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = True
	EndIf
	;Update settings
	$objEmail.Configuration.Fields.Update
	; Set Email Importance
	Switch $s_Importance
		Case "High"
			$objEmail.Fields.Item ("urn:schemas:mailheader:Importance") = "High"
		Case "Normal"
			$objEmail.Fields.Item ("urn:schemas:mailheader:Importance") = "Normal"
		Case "Low"
			$objEmail.Fields.Item ("urn:schemas:mailheader:Importance") = "Low"
	EndSwitch

	$objEmail.Fields.Update
	; Sent the Message
	$objEmail.Send
	If @error Then
		SetError(1)
		Return $o_ComErrorDescription
	Else
		Return 1
	Endif
EndFunc ;==>_INetSmtpMailCom

;===============================================================================
; Function 			_ComErrFunc()
;
; Description:		Com Error Handler
;					This function is called if an AutoIt.Error occurs.
;					If a com-error occurs, the error-number is written to
;					the global variable $s_ComError[0] and the description is written
;					to the global variable $s_ComError[1]
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _ComErrFunc()
    $h_ErrorNumber = Hex($o_ComError.number, 8)
	$o_ComErrorDescription = "Error number " & $h_ErrorNumber & @CRLF & StringStripWS($o_ComError.description, 3)
    SetError(1); something to check for when this function returns
    Return
EndFunc   ;==>MyErrFunc