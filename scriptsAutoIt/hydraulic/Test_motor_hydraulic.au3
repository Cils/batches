;===============================================================================
; Description:		Test if Motor work
;
; Author:			Achille Capelli, SLF Davos
;
;===============================================================================

#include <MailAndSMS.au3>
#include <Date.au3>
#Include <GuiComboBox.au3>


; Flags
Const $b_Test = False
; Titles of windows
Const $s_Station = "Weissfluhjoch"
Const $s_SplashControlText = "AutoIt Measurement"
Const $s_WinK2 = "K2FastWave v02.00.010"
Const $s_WinRadarSetup = "K2FastWave v02.00.010 - RADAR SETUP"
Const $s_WinAcquisition = "K2FastWave v02.00.010 - ACQUISITION SELECTION"
Const $s_WinNewSurvey = "New survey"
Const $s_WinMeas = "K2FastWave v02.00.010 - NEW ACQUISITION"
Const $s_WinAdvantecDM = "Advantech Device Manager"
Const $s_WinUSB = "Advantech Device Test - USB-4761 BoardID=0"
Const $s_WinSavePopup = "K2FastWave"
Const $s_WinSavePopupText = "Save last acquisition?"
; ID's of controls
Const $i_IdStartCalibration = 84
Const $i_IdCurrentSurvey = 54
Const $i_IdNewSurvey = 53
Const $i_IdNewSurveyText = 2
Const $i_IdNewSurveyOK = 3
Const $i_IdNewSurveyCancel = 4
Const $i_IdNewAcquisitionText = 58
Const $i_IdNewAcquisition = 61
Const $i_IdEndAcquisition = 68
Const $i_IdStartScan = 67
Const $i_IdStopScan = 65
Const $i_IdBackToSetup = 88
;Const $i_IdLift = 123
;Const $i_IdFMCW1 = 111
;Const $i_IdFMCW2 = 110
Const $i_IdSave = 6
Const $i_IdDontSave = 7
; Text of controls
Const $s_IdStartCalibrationText = "Start calibration"
Const $s_IdNewSurveyText = "New survey"
Const $s_IdNewAcquisitionText = "New acquisition"
Const $s_IdEndAcquisitionText = "End acquisition"
Const $s_IdStartScanText = "Start scan"
Const $s_IdStopScanText = "Stop scan"
Const $s_IdBackToSetupText = "Back to setup"
Const $s_IdLiftText = ""
Const $s_IdSaveText = "Ja"
Const $s_IdDontSaveText = "Nein"
; Constant numbers
Const $s_MinFreeSpaceC = 10*86.7
Const $s_MinFreeSpaceD = 10*(86.7+86.1)
Const $i_NrOfMeasurements = 2
Const $i_WaitSec = 5
Const $i_MaxNrOfAttempts = 5
; Strings
Const $s_LogError = "Could not open log-file."
; Directories/Files
Const $s_DirK2 = "C:\K2FastWave\mission\"
Const $s_DirAutoIt = "C:\Users\Admin\Desktop\batches\"
Const $s_DirGoogleDrive = "C:\Users\Admin\Google Drive\upGPR WFJ\"
;Const $s_DirUS = "D:\STEINKOGLER\all.ultrasonics\"
;Const $s_DirFMCWMotorDat = "C:\Documents and Settings\steinkog\Desktop\FMCW- Radar\FMCW_Motor_exe_121004\data\upload\"
;Const $s_DirFMCWCMPDat = "C:\Documents and Settings\steinkog\Desktop\FMCW- Radar\FMCW_CMP_exe_121004\data\upload\"
; Paths to Programs
Const $s_PathAdvantecDM = "C:\WINDOWS\DevMgr.exe"
Const $s_PathK2 = "C:\K2FastWave\K2FastWave"
;Const $s_PathRS232DataLogger = "C:\Program Files\Eltima Software\RS232 Data Logger\DataLogger.exe"
; Actual date
Const $s_YEAR = @YEAR
Const $s_MON = @MON
Const $s_DAY = @MDAY
Const $s_HOUR = @HOUR
Const $s_MIN = @MIN
Const $s_now = $s_YEAR & "." & $s_MON  & "." & $s_DAY & " " & $s_HOUR & ":" & $s_MIN
Const $a_MonthsGerman[12] = ["Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]


;Constants for Motor via Advantech
Const $i_AdvDeviceNum = 0
;Const $i_AdvInputPort = 1
;Const $i_AdvPinLiftBusyReady = 0
;Const $i_AdvLiftBusy = False
;Const $i_AdvLiftReady = True
Const $i_AdvOutputPort = 0
Const $i_AdvCombinationLiftHoch = 0x06
Const $i_AdvCombinationLiftRunter = 0x00

;BlockInput(1)
$cSplashWindow = SplashTextOn($s_SplashControlText, "", -1, -1, -1, -1, 4+16, "", 24)
_PopupText("")
_WriteToLogFile("Start new measurement", 1)


; ----------------------------------------------------
; Initialize Advantech and move radar down
; ----------------------------------------------------
 _SetAdvantechOutputs($i_AdvCombinationLiftRunter)




; ----------------------------------------------------
; move radar up and down
; ----------------------------------------------------
For $j = 1 to $i_NrOfMeasurements
	; move radar up
	_PopupText("Round " & $j & ": Move radar up.")
	_WriteToLogFile("Round " & $j & ": Move radar up.")
;~ 	If Not WinExists($s_WinUSB) Then
;~ 		_StopAndExit("USB window does not exist.")
;~ 	EndIf
;~ 	ControlFocus ( $s_WinUSB, $s_IdLiftText, $i_IdLift )
;~ 	ControlClick ( $s_WinUSB, $s_IdLiftText, $i_IdLift )
;~ 	If Not ControlCommand( $s_WinUSB, $s_IdLiftText, $i_IdLift, "IsChecked", "") Then
;~ 		_StopAndExit("Liftcontrol wants not to be checked.")
;~ 	EndIf
    _SetAdvantechOutputs($i_AdvCombinationLiftHoch)
	Sleep(10000)
	_PopupText("Round " & $j & ": Wait on upper position.")
	_WriteToLogFile("Round " & $j & ": Wait on upper position.")
	Sleep(5000)

	; move radar down
	_PopupText("Round " & $j & ": Move radar down.")
	_WriteToLogFile("Round " & $j & ": Move radar down.")
;~ 	If Not WinExists($s_WinUSB) Then
;~ 		_StopAndExit("USB window does not exist.")
;~ 	EndIf
;~ 	ControlFocus ( $s_WinUSB, $s_IdLiftText, $i_IdLift )
;~ 	ControlClick ( $s_WinUSB, $s_IdLiftText, $i_IdLift )
;~ 	If ControlCommand( $s_WinUSB, $s_IdLiftText, $i_IdLift, "IsChecked", "") Then
;~ 		_StopAndExit("Liftcontrol wants not to be unchecked.")
;~ 	EndIf
    _SetAdvantechOutputs($i_AdvCombinationLiftRunter)
	Sleep(20000)
	_PopupText("Round " & $j & ": Wait on lower position.")
	_WriteToLogFile("Round " & $j & ": Wait on lower position.")
	Sleep(5000)
Next


;===============================================================================
; Function			_SetAdvantechOutputs($i_pin, $b_type)
;
; Description:		Send outputs of Advantech
;
; Parameter(s):		$i_pin: on which Pin to Send
; 					$b_type:	False = goTo Low
;								True = goTo High
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _SetAdvantechOutputs($i_output)
;   If $b_type Then
;	  $i_maskOr=0x00
;	  $i_maskAnd=BitNot(0x01*($i_pin+1))
;   Else
;	  $i_maskOr=0x01*($i_pin+1)
;	  $i_maskAnd=-1
;   EndIf
;    $a_dllResult=DllCall("advantechToAutoit.dll", "INT", "readOutput", "INT", $i_AdvDeviceNum, "INT", $i_AdvOutputPort )
;    If @error Or $a_dllResult[0]==-1 Then
; 	  _StopAndExit("Cannot communicate with Advantech USB-4761 (readOutput).")
;    EndIf
;    $i_output = BitOR($a_dllResult[0], $i_maskOr)
;    $i_output = BitAnd($i_output, $i_maskAnd)
   $a_dllResult=DllCall("advantechToAutoit.dll", "BOOL", "setOutput", "INT", $i_AdvDeviceNum, "INT", $i_AdvOutputPort, "BYTE", $i_output )
   If @error Or $a_dllResult[0]==False Then
	  _StopAndExit("Cannot communicate with Advantech USB-4761 (setOutput).")
   EndIf
EndFunc

;===============================================================================
; Function			_ReadAdvantechInputs($i_pin)
;
; Description:		Read input of Advantech
;
; Parameter(s):		$i_pin: pin to read
;
; Return:			boolean: 	True = high
;								False = low
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _ReadAdvantechOutputs()
   $a_dllResult=DllCall("advantechToAutoit.dll", "INT", "readOutput", "INT", $i_AdvDeviceNum, "INT", $i_AdvOutputPort)
   If @error Or $a_dllResult[0]==-1 Then
	  _StopAndExit("Cannot communicate with Advantech USB-4760 (readOutput).")
   EndIf
   Return $a_dllResult[0]
EndFunc

;===============================================================================
; Function			_CheckFMCWFileAndSendMessages($s_FileFMCWFile)
;
; Description:		Check if FMCW-File is older than soundsoviel and write mail, sms and logfile
;
; Parameter(s):		$s_FileFMCWFile: Path to file
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _CheckFMCWFileAndSendMessages($s_FileFMCWFile)
	$a_lastMod =  FileGetTime($s_FileFMCWFile)
	If Not @error Then
		$s_lastMod = $a_lastMod[0] & "." & $a_lastMod[1]  & "." & $a_lastMod[2] & " " & $a_lastMod[3] & ":" & $a_lastMod[4]
		$s_lastClockHour = $s_YEAR & "." & $s_MON  & "." & $s_DAY & " " & $s_HOUR & ":00"
		$i_mSinceLastMod = _DateDiff("n", $s_lastMod, $s_now)
		$i_mSinceLastClockHour = _DateDiff("n", $s_lastClockHour, $s_now)
		If ($i_mSinceLastMod-$i_mSinceLastClockHour>2) Then
			$i_beginFileName = 1+StringInStr ( $s_FileFMCWFile, "\" , 0, -1)
			$s_FileNameFMCWFile=StringMid($s_FileFMCWFile,$i_beginFileName)
			$s_Message = "File '" & $s_FileNameFMCWFile & "' was last modified at " & $s_lastMod & " (expected " & _DateAdd( 'n', -2, $s_lastClockHour) & " - " & $s_lastClockHour & ")."
			_WriteToLogFile($s_Message, 3)
			$s_MailSmsError = _SendMailAndSMS($s_Station, $s_Message, 0, True, True, 0)
			$i_errorCode = @error
			If Not (BitAND($i_errorCode,7) = 0) Then
			   _WriteToLogFile("Could not send Mail and SMS." & @CRLF & $s_MailSmsError, 3)
			ElseIf (BitAND($i_errorCode,8) = 8) Then
			   _WriteToLogFile("Could send Mail and SMS, but " & $s_MailSmsError)
			EndIf
		EndIf
	Else
		_WriteToLogFile("FMCW-File '" & $s_FileFMCWFile & "' could not be checked",3)
		$s_MailSmsError = _SendMailAndSMS($s_Station, "FMCW-File '" & $s_FileFMCWFile & "' could not be checked", 0, True, True, 0)
		$i_errorCode = @error
	    If Not (BitAND($i_errorCode,7) = 0) Then
			_WriteToLogFile("Could not send Mail and SMS." & @CRLF & $s_MailSmsError, 3)
	    ElseIf (BitAND($i_errorCode,8) = 8) Then
			_WriteToLogFile("Could send Mail and SMS, but " & $s_MailSmsError)
	    EndIf
	EndIf
EndFunc

;===============================================================================
; Function			_WriteToLogFile($s_inputStr)
;
; Description:		Write something to logfile
;
; Parameter(s):		$s_inputStr: String to write to logfile
;					$i_mode:	1: as a title
;								2: normalLogEntry
;								3: error
;								4: unplanned incident (unexpected, but program can solve problem and continue)
;								default: 2
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _WriteToLogFile($s_inputStr, $i_mode=2)
	$file = FileOpen("AutoitLog.txt", 1)
	; Check If file opened for writing
	If $file = -1 Then
		_StopAndExit($s_LogError)
	EndIf

	$s_inputStr = StringReplace($s_inputStr,@CRLF," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@CR," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@LF," ./. ")
	$s_timestamp = @MDAY & "." & @MON & "." & @YEAR & " " & @Hour & ":" & @Min & ":" & @SEC
	Switch $i_mode
		Case 1
			$s_inputStr = @CRLF & "  ++++++++++++++++++++++++++++++++++++++" & @CRLF & "  + " & $s_timestamp & @CRLF & "  + " & $s_inputStr & @CRLF & "  ++++++++++++++++++++++++++++++++++++++"
		Case 2
			$s_inputStr = "[" & $s_timestamp & " M] " & $s_inputStr
		Case 3
			$s_inputStr = "[" & $s_timestamp & " E] " & "E R R O R : " & $s_inputStr
		Case 4
			$s_inputStr = "[" & $s_timestamp & " U] " & "UNPLANNED INCIDENT: " & $s_inputStr
	EndSwitch

	FileWriteLine($file, $s_inputStr)
	FileClose($file)
EndFunc

;===============================================================================
; Function			_PopupText($s_ActualAction)
;
; Description:		Update Text to inform user that and which measurement is
;					in process
;
; Parameter(s):		$s_ActualAction: Action that is carried out at the moment
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _PopupText($s_ActualAction)
	$s_Message = "A measurement is in progress. Please do not use mouse or keyboard." & @CRLF & @CRLF & "Actual action: " & @CRLF & $s_ActualAction
	ControlSetText($s_SplashControlText, "", "Static1", $s_Message)
EndFunc

;===============================================================================
; Function        _StopAndExit([$s_ErrorMessage])
;
; Description:		This method can be called if an error occurs.
;					If $s_ErrorMessage not "", an error-message is sent over mail
;					and over SMS.
;					If $s_ErrorMessage not "", a message with the error-message is displayed
;					A running measurement is stopped
;					Exit from program
;
; Parameter(s):		$s_ErrorMessage:	Error-message or "" if no advices over mail,
;										SMS and Messagebox is desired
;										Default: ""
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _StopAndExit($s_ErrorMessage = "")
	; send errorMessage over SMS and Mail
	If Not $s_ErrorMessage="" Then
		$s_MailSmsError = _SendMailAndSMS($s_Station, $s_ErrorMessage)
		$i_errorCode = @error
	    If Not (BitAND($i_errorCode,7) = 0) Then
		    _WriteToLogFile("Could not send Mail and SMS." & @CRLF & $s_MailSmsError, 3)
	    ElseIf (BitAND($i_errorCode,8) = 8) Then
		    _WriteToLogFile("Could send Mail and SMS, but " & $s_MailSmsError)
	    EndIf
	EndIf
	; log error
	If Not ($s_ErrorMessage = $s_LogError) Then
		 _WriteToLogFile("Program stops with error message: " & $s_ErrorMessage, 3)
	 EndIf
	; stop scan
	If WinExists($s_WinMeas) And ControlCommand($s_WinMeas, $s_IdStopScanText, $i_IdStopScan, "IsEnabled", "") Then
		ControlFocus ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
		ControlClick ( $s_WinMeas, $s_IdStopScanText, $i_IdStopScan )
		; wait for save-popup and press no
		WinWait($s_WinSavePopup, $s_WinSavePopupText, $i_WaitSec)
		If WinExists($s_WinSavePopup, $s_WinSavePopupText) Then
			ControlClick ( $s_WinSavePopup, $s_IdDontSaveText, $i_IdDontSave )
		EndIf
	EndIf
;~ 	; stop logging ultrasonics
;~ 	_StartSingleRS232DataLogging($s_UsCom1, $s_UsFile1, $s_PathRS232DataLogger)
	; show error message
	If Not $s_ErrorMessage="" Then
		SplashOff()
		MsgBox( 0x1010, "Error", "AutoIt abandoned the measurement." & @CRLF & @CRLF & $s_ErrorMessage, 3*$i_WaitSec)
	EndIf
	; exit
	Exit
EndFunc