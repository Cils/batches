
echo start resetNumato
echo ------------------------------ >> C:\batches\AutoitLog.txt
echo start resetNumato on %date% at %time% >> C:\batches\AutoitLog.txt
echo ------------------------------ >> C:\batches\AutoitLog.txt
timeout /T 10  /NOBREAK

echo disable now
echo disable now >> C:\batches\AutoitLog.txt

# Get status Numato
#Get-PnpDevice -FriendlyName "Numato*"
# Get InstanceId
#Get-PnpDevice -FriendlyName "Numato*"| ft -wrap -autosize friendlyname, instanceid

# Disable
Disable-PnpDevice -InstanceId "USB\VID_2A19&PID_0800\5&22AA7556&0&4" -confirm:$false


timeout /T 10  /NOBREAK


echo enable now
echo enable now >> C:\batches\AutoitLog.txt
#Enable
Enable-PnpDevice -InstanceId "USB\VID_2A19&PID_0800\5&22AA7556&0&4"  -confirm:$false

timeout /T 10  /NOBREAK

echo Start Autoit >> C:\batches\AutoitLog.txt
echo Start Autoit
# & "C:\batches\PRM_Wfj_v2.exe"

echo AutoIt finished at %time% >> C:\batches\AutoitLog.txt
echo resetNumato finished at %time% >> C:\batches\AutoitLog.txt
echo " " >> C:\batches\AutoitLog.txt