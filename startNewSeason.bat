REM --- Start python file for initialising new season for the upGPR ---
python C:\batches\startNewSeason.py

@echo off

if ERRORLEVEL 1 (
	C:\batches\SendMail.exe "Creation of New season failed. Error in python script: C:\batches\startNewSeason.py" 0
	echo "Creation of New season failed. Error in python script: C:\batches\startNewSeason.py"
	)
	
@echo on