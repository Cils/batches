REM --- Copy files to phoenix ---

@echo off

cd C:\batches

REM --- Read settings from upGPR.ini file ---
for /f  %%i in ('c:\batches\ini.bat /s general /i station c:\batches\upGPR.ini') do set station=%%i
for /f %%i in ('c:\batches\ini.bat /s general /i season c:\batches\upGPR.ini') do set season=%%i

set station=%station:"=%
set season=%season:"=%

set "scans=D:\scans\%season%"
set phradar="X:\Radar\Radardata\%season%\upGPR %station%"
set phradar2="%phradar:"=%\missions"

echo %phradar%
echo %phradar2%
echo %scans%

REM --- connect network drive (necessary after startup)
set /A a=0
:Start
	timeout /t 5
	if exist %phradar% 	goto End

	REM try 5 times
	set /A a+=1
	if %a%==6 (
		set msg = "Unable to reconnect to \\phoenix\lbi"
		C:\batches\SendMail.exe "Unable to reconnect to \\phoenix\lbi" 0
		echo %msg%
		
		goto End)

	REM reconnect to Phoenix
	net use X: \\phoenix\lbi  /PERSISTENT:YES
	if ERRORLEVEL 1  goto Start
:End



REM --- Copy files to phoenix ---
robocopy %scans%  %phradar2% /s /maxage:30 /copy:DT /dcopy:DT /r:2 /w:10 /xf RobocopyLog.log DataFloats.dat /log+:"%scans%\RobocopyLog.log"  /ns /fp /np /ndl


IF %ERRORLEVEL% gtr 7 (
	set msg = "Copy of upGPR data transfer was unsuccessfully. Robocopy error!"
	C:\batches\SendMail.exe "Copy of upGPR data transfer was unsuccessfully. Robocopy error!" 0
	echo %msg%
)

echo "Robocopy output:"
echo %ERRORLEVEL% 


REM --- Copy Logfiles Autoit and Robocopy ---
robocopy C:\batches %phradar%  AutoitLog.txt  /copy:DT
robocopy %scans%  %phradar%  RobocopyLog.log  /copy:DT
robocopy %scans%  %phradar%  DataFloats.dat  /copy:DT

@echo on


REM check if new data were created today
set today_dir= %scans%\%date:~-2%%date:~3,2%%date:~0,2%.Mis
echo %today_dir%

if exist %today_dir% (
	echo %date% >> "X:\Radar\Radardata\icinga_WFJ.log"
	echo New scans exists for today.  %today_dir% >> "X:\Radar\Radardata\icinga_WFJ.log"
	echo ok: New scans exists for today. %today_dir%
) else (	
	set msg = "No scans exists for today. UpGPR scan script not working properly!"
	C:\batches\SendMail.exe "No scans exists for today. UpGPR scan script not working properly!" 0
	echo %msg%
)



REM ---- update logfile for monitoring PC with icinga ---------
REM echo %date% >> "X:\Radar\Radardata\icinga_WFJ.log"
echo icinga.slf.local_lastupdated, %date% >> "X:\Radar\Radardata\icinga_WFJ.log"

REM pause
