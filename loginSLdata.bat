REM --- Copy files to phoenix ---

@echo off

cd C:\batches

set SLdata="P:\"

echo %SLdata%

REM --- connect network drive (necessary after startup)
set /A a=0
:Start
	timeout /t 5
	if exist %SLdata% 	goto End

	REM try 5 times
	set /A a+=1
	if %a%==6 (
		set msg = "Unable to reconnect to \\memobi3.wsl.ch\pub\SLdata"
		C:\batches\SendMail.exe "Unable to reconnect to \\memobi3.wsl.ch\pub\SLdata" 0
		echo %msg%
		
		goto End)

	REM reconnect to Phoenix
	net use P: \\memobi3.wsl.ch\pub\SLdata  /PERSISTENT:YES
	if ERRORLEVEL 1  goto Start
:End