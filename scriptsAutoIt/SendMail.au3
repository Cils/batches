;===============================================================================
; Description:		Write Error email email 
; 			
; Parameters:		
;				Parameter1:		String to send as Error Message
;				Parameter2:		type of error see: MailandSMS.au3, default: 1 = Information message
;	e.g: 
;
; Author:			Lino Schmid, SLF Davos
; Modified by:		Achille Capelli, SLF Davos, 2019
;===============================================================================

#include <MailAndSMS.au3>
#include <Date.au3>
#Include <GuiComboBox.au3>
#include <Timers.au3>
#include "CommAPI/CommInterface.au3"

; Path of .ini file containing setting that can change
Const $s_Inifile = "C:\batches\upGPR.ini"

; settings from .ini file
Const $s_Season = IniRead ( $s_Inifile, "general", "Season", "NoSeason" )
Const $s_MailSender  = IniRead ( $s_Inifile, "AutoIt", "MailSender", "vanhervijnen@slf.ch" )
Const $s_MailReceiver  = IniRead ( $s_Inifile, "AutoIt", "MailReceiver", "capelli@slf.ch" )
Const $s_Station = IniRead ( $s_Inifile, "general", "Station", "Weissfluhjoch")

Const $s_DirAutoItLog = "C:\batches\AutoitLog.txt"


; Get parameters passed by commandline
If $CmdLine[0] >= 1 Then
	$s_Message= $CmdLine[1]
Else
	$s_Message= "Unknown message"
EndIf

If $CmdLine[0] >= 2 Then
	$i_type= $CmdLine[2]
Else
	$i_type=1
EndIf



; ----------------------------------------------------
; send error messages
; ----------------------------------------------------
$s_MailSmsError=_SendMail($s_Station, $s_Message, $i_type,  $s_MailReceiver,$s_MailSender)
$i_errorCode = @error
If Not (BitAND($i_errorCode,7) = 0) Then
	_WriteToLogFile("Could not send Mail." & @CRLF & $s_MailSmsError, 3, $s_DirAutoItLog)
	SetError(1)
	Return "Could not send Mail"
 ElseIf (BitAND($i_errorCode,8) = 8) Then
	_WriteToLogFile("Could send Mail but " & $s_MailSmsError, 2, $s_DirAutoItLog)
 ElseIf  $s_MailSmsError <> 1 Then
	_WriteToLogFile("Could not send Mail." & @CRLF & $s_MailSmsError, 3, $s_DirAutoItLog)
	SetError(1)
	Return "Could not send Mail"
EndIf


Exit


;===============================================================================
; Function			_WriteToLogFile($s_inputStr)
;
; Description:		Write something to logfile
;
; Parameter(s):		$s_inputStr: String to write to logfile
;					$i_mode:	1: as a title
;								2: normalLogEntry
;								3: error
;								4: unplanned incident (unexpected, but program can solve problem and continue)
;								default: 2
;
; Author:			Lino Schmid, SLF Davos
;
;===============================================================================
Func _WriteToLogFile($s_inputStr, $i_mode=2,$s_filepath="AutoitLog.txt")

	$file = FileOpen($s_filepath, 1)
	; Check If file opened for writing
	If $file = -1 Then
		SetError(2)
		Return "Could not open LogFile"
	EndIf

	$s_inputStr = StringReplace($s_inputStr,@CRLF," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@CR," ./. ")
	$s_inputStr = StringReplace($s_inputStr,@LF," ./. ")
	$s_timestamp = @MDAY & "." & @MON & "." & @YEAR & " " & @Hour & ":" & @Min & ":" & @SEC
	Switch $i_mode
		Case 1
			$s_inputStr = @CRLF & "  ++++++++++++++++++++++++++++++++++++++" & @CRLF & "  + " & $s_timestamp & @CRLF & "  + " & $s_inputStr & @CRLF & "  ++++++++++++++++++++++++++++++++++++++"
		Case 2
			$s_inputStr = "[" & $s_timestamp & " M] " & $s_inputStr
		Case 3
			$s_inputStr = "[" & $s_timestamp & " E] " & "E R R O R : " & $s_inputStr
		Case 4
			$s_inputStr = "[" & $s_timestamp & " U] " & "UNPLANNED INCIDENT: " & $s_inputStr
	EndSwitch

	FileWriteLine($file, $s_inputStr)
	FileClose($file)
EndFunc