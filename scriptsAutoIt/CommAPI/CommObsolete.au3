; #INDEX# =======================================================================================================================
; Name ..........: CommObsolete.au3
; Version Date ..: 2014-04-07
; AutoIt Version : 3.3.8.1
; ===============================================================================================================================
 
#include-once
#include "CommInterface.au3"
#NoAutoIt3Execute
#AutoIt3Wrapper_Au3Check_Parameters=-q -d -w 1 -w 2 -w 3 -w- 4 -w 5 -w 6 -w- 7
 
Func _CommAPI_ReceiveData(Const $hFile, Const $iTimeout = 1, Const $iMaxLength = 0)
	Local $sResult = _CommAPI_ReceiveString($hFile, $iTimeout, $iMaxLength)
	If @error Then SetError(@error, @extended, $sResult)
	Return $sResult
EndFunc
 
Func _CommAPI_ReceiveLine(Const $hFile, Const $sSeparator = @CRLF, Const $iTimeout = 1, Const $iMaxLength = 0)
	Local $sResult = _CommAPI_ReceiveString($hFile, $iTimeout, $iMaxLength, $sSeparator)
	If @error Then SetError(@error, @extended, $sResult)
	Return $sResult
EndFunc
 
Func _CommAPI_TransmitData(Const $hFile, Const $vData)
	Local $iWritten = _CommAPI_TransmitBinary($hFile, $vData)
	If @error Then Return SetError(@error, @extended, $iWritten)
	Return $iWritten
EndFunc